<%
Yii::app()->clientScript->registerScriptFile('/libs/redactorjs/ru.js');
Yii::app()->clientScript->registerScriptFile('/libs/django-urlify/urlify.js');
Yii::app()->clientScript->registerScript('translit', "
$('#translit-btn').click(function() {
	$('#Page_slug').val(URLify($('#Page_page_title').val()));
});
");
%>

<% if (!$model->isNewRecord): %>
<div class="control-group">
    <% echo $form->labelEx($model, 'url', array('class' => 'control-label')) %>
    <div class="controls">
        <a target="_blank" style="display: block; width: 100%; overflow: hidden; padding-top: 4px" href="<% echo $this->createUrl('/pages/default/view', array('id' => $model->id))%>">
            <% echo $this->createUrl('/pages/default/view', array('id' => $model->id))%>
        </a>
    </div>
</div>
<% endif; %>

<% echo $form->textFieldRow($model, 'module', array('class' => 'span6', 'maxlength' => 255)) %>

<% echo $form->dropDownListRow($model, 'parent_id', $model->selectList(), array('class' => 'span6', 'empty' => '')) %>
<% echo $form->textFieldRow($model, 'page_title', array('class' => 'span6', 'maxlength' => 255)) %>


<div class="control-group">
	<% echo $form->labelEx($model, 'slug', array('class' => 'control-label', 'label' => 'Псевдоним')) %>
	<div class="controls">
		<div class="input-append">
			<% echo $form->textField($model, 'slug', array('class' => 'span5', 'maxlength' => 127)) %><button class="btn" type="button" id="translit-btn">Транслит</button>
		</div>
	</div>
</div>

<div class="control-group">

	<div class="controls">
        <label for="" class="inline">
            <% echo $form->checkBox($model, 'is_published') %>
           	<% echo $form->labelEx($model, 'is_published', array('class'=>'lbl')) %>
        </label>
	</div>
</div>

<%/* echo $form->dropDownListRow($model, 'layout', array(
	'column1' => 'Одна колонка',
	'column2' => 'Две колонки',
), array('empty' => 'По умолчанию', 'class' => 'span3'))*/ %>

<%if( $model->type == Page::TYPE_PAGE): %>
<div class="control-group">
	<% echo $form->labelEx($model, 'content', array('class' => 'control-label')) %>
	<div class="controls">
		<% $this->widget('ext.imperavi-redactor.ImperaviRedactorWidget', array(
			'model' => $model,
			'attribute' => 'content',
			'options' => array(
				'lang' => 'ru',
			),
		)) %>
	</div>
</div>
<% endif;%>