<?php

class DefaultController extends CoreController
{
    public  $baseModel = 'BlogPost';
    public $crudFormID = 'blog-post-form';
    public $multiLang  = true;

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view'),
                'users' => array('?'),
            ),
            array('allow', 'users' => array('@')),
            array('deny',  'users' => array('*') ),
        );
    }


    public function actionIndex($category = '', $post = '', $page = 0)
    {
        $pageSize = 5;
        $page = ($page == 0 || $page == '') ? 0 : $page - 1;

        $this->layout = 'application.views.layouts.main';

        $pageModel = Page::model()->findByAttributes(
            array(
                'module' => 'blog',
                'lang' => Yii::app()->language)
        );
        if(!empty($pageModel)) {
            $criteria = new CDbCriteria;

            $criteria->with  =  array('category');
            $criteria->order = '`created_at` DESC';
            $criteria->condition = 'visible = 1';
            $criteria->offset = $page * $pageSize;

            if (!empty($post)) {
                $criteria->addSearchCondition('b.id', $post, 'AND');
            }
            if (!empty($category)) {
                $criteria->addSearchCondition('name', $category, 'AND');
            }

            $dataProvider = new CActiveDataProvider(
                $this->baseModel,
                array(
                    'criteria'=> $criteria,
                    'pagination'=>array(
                        'pageSize' => $pageSize,
                        'pageVar'=>'page',
                    )
                )
            );

            if (!Yii::app()->request->getIsAjaxRequest()) {
                $dataProvider->setPagination(
                    array('currentPage' => $page)
                );
            }

            $meta = Meta::model()->findByPk( Yii::app()->name  . '(' . Yii::app()->language . ')');
            $content = $this->renderPartial('index', compact('dataProvider', 'page'), true, true);
            $pageModel->content = $content;

            $this->render(Yii::getPathOfAlias('page-view'), array('page' => $pageModel, 'meta' => $meta));
        } else {
            throw new CHttpException(404, 'Blog Page Not Created');
        }
    }

    public function actionView($id)
    {
        $this->layout = 'application.views.layouts.main';
        $page = Page::model()->findByAttributes(array('module' => 'blog'));
        $data = BlogPost::model()->findByPk($id);
        $meta = Meta::model()->findByPk( Yii::app()->name  . '(' . Yii::app()->language . ')');
        $content = $this->renderPartial('view', compact('data'), true, true);
        $page->content = $content;

        $this->render(Yii::getPathOfAlias('page-view'), compact('page', 'meta'));
    }

}