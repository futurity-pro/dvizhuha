<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(t('Category'), array('/blog/categoryAdmin/admin'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo t('Create Item')%>
        </small>
    </h1>
</div>
<% $this->renderPartial('_form', array('model' => $model)); %>