<?php

class m140221_083540_add_template_for_meta extends CDbMigration
{

	public function safeUp() {
        $this->addColumn('meta', 'meta_template', 'text NULL');
	}

	public function safeDown() {
        $this->dropColumn('meta', 'meta_template');
	}

}