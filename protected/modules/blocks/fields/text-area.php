<?php
/**
 * @file text-area.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 27.02.14 20:26
 */
?>
<div class="control-group">
    <% echo CHtml::activeLabel($model, $name , array('class' => 'control-label')); %>
    <div class="controls">
        <% echo CHtml::activeTextArea($model, $name , array('rows'=>6, 'cols'=>50)); %>
        <% echo CHtml::error($model, $name, array('class' => 'text-error')); %>
    </div>
</div>
