<?php
/**
 * @file core.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 12.02.14 21:09
 */
return array(
    'Manage Blog' => 'Управление Блогом',
    'Manage Category' => 'Управление Категориями',
    'Blog' => 'Блог',
);