<div class="control-group">
    <% echo CHtml::activeLabel($model, 'content', array('class' => 'control-label')) %>
    <div class="controls">
        <% $this->widget('ext.imperavi-redactor.ImperaviRedactorWidget', array(
            'model' => $model,
            'attribute' => $name,
            'options' => array(
                'lang' => 'ru',
            ),
        )) %>
    </div>
</div>