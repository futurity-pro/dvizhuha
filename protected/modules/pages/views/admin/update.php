<%
$this->pageTitle = $model->page_title;
$this->breadcrumbs = array(
	'Статические страницы' => array('index'),
	$this->pageTitle,
);
%>
<div class="page-header position-relative">
    <h1>
        <% echo t('Edit Item'); %>
    </h1>
</div>
<% $this->renderPartial('_form', array('model' => $model)) %>