<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('cabinet-widgets', dirname(__FILE__).'/../extensions/cabinet');
require_once('protected/helpers/common.php');


function t($message, $params = array()) {
    return Yii::t('core', $message, $params);
}
$env = getenv('APPLICATION_ENV');
return array(
    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name' => 'Движуха',
    //'defaultController' => 'site/index',
    'language' => 'ru',
    'sourceLanguage' => 'en',

    //'preload'=> ($env == 'dev') ? array('log') : array(''),

    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.components.widgets.*',
        'application.components.yii-mail.*',
        'application.modules.advantage.models.*',
        'application.modules.pages.models.*',
        'application.modules.portfolio.models.*',
        'application.modules.review.models.*',
        'application.modules.customfields.models.*',
        'application.modules.menumanager.models.*',
        'application.extensions.mailer.ajEMailer',
    ),

    // modules setup section
    'modules' => array_merge (
        require dirname(__FILE__) . DIRECTORY_SEPARATOR .  'data' . DIRECTORY_SEPARATOR . 'modules_default.php',
        require dirname(__FILE__) . DIRECTORY_SEPARATOR .  'data' . DIRECTORY_SEPARATOR . 'modules_custom.php'
    ),

    'components'=>array(
        'user'=>array(
            'allowAutoLogin' => true,
        ),

        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'viewRenderer'=>array(
            'class'=>'CPradoViewRenderer',
        ),

        // prevent ajax linking
        'clientScript'=>array(
            'class' => 'ext.NLS.NLSClientScript',
            'mergeJs' => false, //def:true
            'compressMergedJs' => false, //def:false
            'mergeCss' => false, //def:true
            'compressMergedCss' => false, //def:false
            'mergeJsExcludePattern' => '/edit_area/', //won't merge js files with matching names
            'mergeIfXhr' => true, //def:false, if true->attempts to merge the js files even if the request was xhr (if all other merging conditions are satisfied)
            'serverBaseUrl' => 'http://localhost', //can be optionally set here
            'mergeAbove' => 1, //def:1, only "more than this value" files will be merged,
            'curlTimeOut' => 10, //def:10, see curl_setopt() doc
            'curlConnectionTimeOut' => 10, //def:10, see curl_setopt() doc
            //'appVersion'=>1.0 ,//if set, it will be appended to the urls of the merged scripts/css
            'packages' => require dirname(__FILE__) . DIRECTORY_SEPARATOR .  'packages.php',
        ),

        'urlManager'=>array(
            //'class'=>'application.components.UrlManager',
            'urlFormat'		 =>'path',
            'showScriptName'  => false,
            'rules'=>array(
                array('class'=>'application.modules.pages.components.PagesUrlRule'),


                #blog routing module
                'blog/page/<page>/*' => 'blog/default/index',
                'blog/<id:\d+>/<alias:[\w_-\s]+>' => 'blog/default/view',
                'blog/category/<category:[\w_-\s]+>' => 'blog/default/index',


                # Routing for modules
                '<language:(ru|uk|en)>/<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<language:(ru|uk|en)>/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<language:(ru|uk|en)>/<module:\w+>'=>'<module>',

                '<language:(ru|uk|en)>/' => 'site/index',
                #'<language:(ru|uk|en)>/<action:(contact|login|logout)>/*' => 'site/<action>',
                '<language:(ru|uk|en)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<language:(ru|uk|en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<language:(ru|uk|en)>/<controller:\w+>/<action:\w+>/*' => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

                'login' => 'site/login',
                'logout' => 'site/logout',
                'map' => 'site/map',
                'contact' => 'site/contact',
                'admin' => 'pages/admin',



            ),
        ),

        // Setup your DB : Please copy  db.php.dist to db.php and
        'db'  => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'db.php',

        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters'=>array('127.0.0.1','192.168.1.215'),
                ),
            ),
        ),

        // caching
        'cache'=>array(
            'class'=>'CFileCache',
        ),
        'mailer'            => require dirname(__FILE__) . DIRECTORY_SEPARATOR .  'mail.php',
        'mail' => array(
            'class' => 'application.components.yii-mail.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),
    ),

    // using Yii::app()->params['paramName']
    'params'=>array(
        'languages'=>array('ru'=>'Русский', 'uk'=>'Українська', 'en'=>'English'),
        'adminEmail'=>'jehkinen@ya.ru',
    ),
);