
<% echo CHtml::link('Ко всем постам', array('/blog'));%>
<div class="blog__one-post">
    <h3>
        <% echo $data->title %>
    </h3>
    <div class="blog-panel">
        <div class="publication-date pull-left">
            <?php echo CHtml::encode($data->created_at); ?>
        </div>

        <div class="blog-category pull-left">
            <% echo CHtml::link(
                $data->category->name,
                '/' . Yii::app()->request->getPathInfo(). '?category=' . $data->category->name,
                array('class'=>'pull-left')
            ) %>
        </div>
        <div class="clearfix"></div>
    </div>
    <% if(!Yii::app()->user->isGuest) :%>
        <% echo CHtml::link(
            t('Edit'),
            array('/blog/default/update', 'id' => $data->id),
            array('class'=>'pull-left', 'target' => '_blank')
        ) %>
        <div class="clearfix"></div>
    <% endif; %>
    <div class="post-content">
        <?php echo $data->text; ?>
    </div>

    <% $postUrl =  Yii::app()->request->hostInfo . '/' .  Yii::app()->request->getPathInfo() . '?post=' . $data->id; %>
    <div id="vk_like_<% echo $data->id%>"></div>
    </br>
    <div class="fb-like" data-href="<% echo $postUrl%>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
    <script type="text/javascript">
        VK.Widgets.Comments("vk_comments_<% echo $data->id%>", { limit: 10, width: "300px", attach: '*', page_id : '<% echo $data->id %>', pageUrl: '<% echo $postUrl %>' });
        VK.Widgets.Like("vk_like_<% echo $data->id%>", {type: "button", pageUrl: '<% echo $postUrl %>'});
    </script>

    <div class="col-md-6" style="margin: 0">
        <div class="row">
            <div class="fb-comments" data-href="<% echo $postUrl %>" data-numposts="5" data-colorscheme="light" width="250px"></div>
        </div>
    </div>
    <div class="col-md-6" style="margin: 0">
        <div class="row">
            <div id="vk_comments_<% echo $data->id%>"></div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>