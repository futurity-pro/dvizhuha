<?php
class User extends CActiveRecord
{
    CONST ROLE_ADMIN = 1;
    CONST ROLE_USER  = 2;

    public function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return array(
            array('email, password, first_name, last_name', 'required'),
            array('role', 'numerical', 'integerOnly' => true),
            array('email, password', 'length', 'max' => 100),
            array('phone', 'length', 'max' => 30),
            array('joined_on, role', 'safe'),

            array('id, email, phone, password, joined_on, role',
                  'safe', 'on' => 'search'),

            array('joined_on', 'default',
                  'value' => new CDbExpression('NOW()'),
                  'setOnEmpty' => false,'on' => 'create'),
        );
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' 		=> 'ID',
            'email' 	=> 'Email',
            'phone' 	=> 'Телефон',
            'password' 	=> 'Пароль',
            'joined_on' => 'Дата регистрации',
            'role' 		=> 'Роль',
        );
    }

    public function search()
    {

        $criteria=new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('joined_on', $this->joined_on, true);
        $criteria->compare('role', $this->role);

        return new CActiveDataProvider($this, array('criteria' => $criteria));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->registered = date('Y-m-d H:i:s');
                $this->password = self::hashPassword($this->password);
            }
            return true;
        }
        return false;
    }

    static function hashPassword($password)
    {
        return md5($password);
    }
}
