<?php

class m140221_104456_add_templates_for_another_meta_fields extends CDbMigration
{
    public function up()
    {
        $transaction = $this->getDbConnection()->beginTransaction();
        try {
            $sql = "
                ALTER TABLE  `meta` CHANGE  `meta_template`  `meta_template_title` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
                ALTER TABLE  `meta` ADD  `meta_template_description` TEXT NULL , ADD  `meta_template_keywords` TEXT NULL ;
            ";
            $this->execute($sql);
            $transaction->commit();
        }
        catch(Exception $e) {
            echo "Exception: ".$e->getMessage()."\n";
            $transaction->rollBack();
            return false;
        }
    }

    public function down()
    {

    }
}