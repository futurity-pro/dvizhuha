<?php

class m140224_110102_add_lang_field_to_meta extends CDbMigration
{
    public function safeUp() {
        $this->addColumn('meta', 'lang', 'varchar(2) NOT NULL');
    }

    public function safeDown() {
        $this->dropColumn('meta', 'lang');
    }
}