<?php
class ContactForm extends CFormModel
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $message;

    public function rules()
    {
        return array(
            array('name, email, subject, phone, message', 'required'),
            array('email', 'email'),
        );
    }
}