<?php
/**
 * @file common.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 15.02.14 2:34
 */

function tm($message, $module = null, $lang = NULL) {
    if ($module === null) {

        if (Yii::app()->controller->module) {
            $module =  Yii::app()->controller->module->id;
            return Yii::t( ucfirst($module) . 'Module' . '.' . 'core', $message, array(), NULL, $lang);
        }

    }
    return Yii::t( $module, $message, array(), NULL, $lang);
}

function getCurrentModuleName()
{
    if (Yii::app()->controller->module) {
        return Yii::app()->controller->module->id;
    } else {
        return false;
    }
}

function getDefaultModuleAction()
{
    if (Yii::app()->controller->module) {
        return Yii::app()->controller->defaultAction;
    } else {
        return false;
    }
}

function imageResize(
    $source_path,
    $destination_path,
    $newwidth,
    $newheight = FALSE,
    $quality = FALSE // качество для формата jpeg
) {

    ini_set("gd.jpeg_ignore_warning", 1); // иначе на некотоых jpeg-файлах не работает

    list($oldwidth, $oldheight, $type) = getimagesize($source_path);

    switch ($type) {
        case IMAGETYPE_JPEG: $typestr = 'jpeg'; break;
        case IMAGETYPE_GIF: $typestr = 'gif' ;break;
        case IMAGETYPE_PNG: $typestr = 'png'; break;
    }
    $function = "imagecreatefrom$typestr";
    $src_resource = $function($source_path);

    if (!$newheight) { $newheight = round($newwidth * $oldheight/$oldwidth); }
    elseif (!$newwidth) { $newwidth = round($newheight * $oldwidth/$oldheight); }
    $destination_resource = imagecreatetruecolor($newwidth,$newheight);

    imagecopyresampled($destination_resource, $src_resource, 0, 0, 0, 0, $newwidth, $newheight, $oldwidth, $oldheight);

    if ($type = 2) { # jpeg
        imageinterlace($destination_resource, 1); // чересстрочное формирование изображение
        if ($quality) imagejpeg($destination_resource, $destination_path, $quality);
        else imagejpeg($destination_resource, $destination_path);
    }
    else { # gif, png
        $function = "image$typestr";
        $function($destination_resource, $destination_path);
    }

    imagedestroy($destination_resource);
    imagedestroy($src_resource);
}


