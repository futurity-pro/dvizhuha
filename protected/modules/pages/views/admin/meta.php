<div class="page-header position-relative">
    <h1>
        <% echo t('Meta'); %>
    </h1>
</div>

<div class="form-horizontal">

    <% $form=$this->beginWidget('CActiveForm', array(
        'id'=>'meta-form',
        'enableAjaxValidation' => true,
    )); %>

    <div class="control-group">
        <% echo $form->labelEx($model, 'title', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); %>
        </div>
        <% echo $form->error($model,'title'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'description', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'description'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'keywords', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'keywords',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'keywords'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'meta_template_title', array('class' => 'control-label')); %>
        <div class="controls">
            <% echo $form->textField($model,'meta_template_title'); %>
        </div>
        <% echo $form->error($model,'meta_template_title'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'meta_template_description', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'meta_template_description',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'meta_template_description'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'meta_template_keywords', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'meta_template_keywords',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'meta_template_keywords'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'admin_email', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textField($model,'admin_email',array('size'=>60,'maxlength'=>100)); %>
        </div>
        <% echo $form->error($model,'admin_email'); %>
    </div>


    <h3 class="header smaller lighter blue">
        <% echo t('Custom code')%>
    </h3>

    <div class="control-group">
        <% echo $form->labelEx($model, 'cm_head', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'cm_head',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'customCodeHead'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'cm_after_open_body', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'cm_after_open_body',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'cm_after_open_body'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'cm_before_close_body', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'cm_before_close_body',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'cm_before_close_body'); %>
    </div>

    <div class="control-group">
        <% echo $form->labelEx($model, 'cm_footer', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'cm_footer',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'cm_footer'); %>
    </div>

     <div class="control-group">
        <% echo $form->labelEx($model, 'robotsTxt', array('class'=>'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'robotsTxt',array('rows'=>6, 'cols'=>50)); %>
        </div>
        <% echo $form->error($model,'robotsTxt'); %>
    </div>

    <h3 class="header smaller lighter blue">
        <% echo tm('Redirects')%>
    </h3>

    <div class="control-group">
        <% echo $form->labelEx($model, 'redirects', array('class' => 'control-label')); %>
        <div class="controls">
            <% echo $form->textArea($model,'redirects',array('rows' => 6, 'cols' => 50)); %>
        </div>
        <% echo $form->error($model,'redirects'); %>
    </div>

    <div class="clearfix"></div>

    <div class="form-actions">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            <% echo ($model->isNewRecord) ? t('Create') : t('Save'); %>
        </button>
    </div>

    <% $this->endWidget(); %>
</div>

