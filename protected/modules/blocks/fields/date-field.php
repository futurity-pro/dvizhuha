<?php
/**
 * @file text-field.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 27.02.14 19:31
 */
?>
<div class="control-group">
    <% echo CHtml::activeLabel($model, $name , array('class' => 'control-label')); %>
    <div class="controls">
        <% echo CHtml::activeTextField($model, $name,
            array(
                'class'=>'date-picker',
                'data-date-format' => 'dd/mm/yyyy',
                'value' => $model->$name)
        ); %>
        <% echo CHtml::error($model, $name, array('class' => 'text-error')); %>
    </div>
</div>

<script>
    $('.date-picker').datepicker({

    }).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
</script>