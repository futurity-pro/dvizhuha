<%
$this->pageTitle        = $page->page_title;
$this->metaTitle        = $meta->getMetaTitle($page->meta_title);
$this->metaDescription  = $meta->getMetaDescription($page->meta_description);
$this->metaKeywords     = $meta->getMetaKeywords($page->meta_keywords);
$this->breadcrumbs      = $page->breadcrumbs;
%>

<div class="homepage homepage-bg">
    <div class="container">
        <div class="container-bg">
            <div class="internal">
                <% echo $page->cm_before_content %>
                    <!--h1><% echo $page->page_title %></h1-->
                    <% echo $page->content %>
                <% echo $page->cm_after_content %>
            </div>
        </div>
    </div>
</div>


