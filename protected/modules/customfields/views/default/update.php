<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Custom Fields'), array('/customfields'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo tm('Create Item')%>
        </small>
    </h1>
</div>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>