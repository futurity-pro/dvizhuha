<?php
/**
 * @file languageSelector.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 18.02.14 14:29
 */
?>
<div class="language" id="language-select">
    <?php

    if(sizeof($languages) < 4) { // если языков меньше четырех - отображаем в строчку
        // Если хотим видить в виде флагов то используем этот код
        echo CHtml::openTag('ul');
        foreach($languages as $key=>$lang) {

            if($key != $currentLang) {
//                echo CHtml::link(
//                    '<img src="/images/'.$key.'.gif" title="'.$lang.'" style="padding: 1px;" width=16 height=11>',
//                    $this->getOwner()->createMultilanguageReturnUrl($key));
                echo CHtml::openTag('li');
                   // echo CHtml::link($key , $this->getOwner()->createMultilanguageReturnUrl($key), array('title' => $lang));
                    echo CHtml::link($key , $key, array('title' => $lang));
                echo CHtml::closeTag('li');

            };

        }
        echo CHtml::closeTag('ul');
        // Если хотим в виде текста то этот код
        /*
        $lastElement = end($languages);
        foreach($languages as $key=>$lang) {
            if($key != $currentLang) {
                echo CHtml::link(
                     $lang,
                     $this->getOwner()->createMultilanguageReturnUrl($key));
            } else echo '<b>'.$lang.'</b>';
            if($lang != $lastElement) echo ' | ';
        }
        */
    }
    else {
        // Render options as dropDownList
        echo CHtml::form();
        foreach($languages as $key=>$lang) {
            echo CHtml::hiddenField(
                $key,
                $this->getOwner()->createMultilanguageReturnUrl($key));
        }
        echo CHtml::dropDownList('language', $currentLang, $languages,
            array(
                'submit'=>'',
            )
        );
        echo CHtml::endForm();
    }
    ?>
</div>