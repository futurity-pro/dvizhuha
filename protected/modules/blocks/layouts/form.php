<div class="form-horizontal">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => $this->crudFormID,
        'enableAjaxValidation' => true,
    )); ?>

    <% echo $content;%>

    <?php $this->endWidget(); ?>
</div><!-- form -->