<?php

class CustomField extends CActiveRecord
{

	public function tableName()
	{
		return 'custom_fields';
	}

	public function rules()
	{
		return array(
			array('field_key, value, title', 'required'),
			array('field_key', 'unique'),
			array('field_key, title', 'length', 'max'=>100),
			array('value', 'length', 'max'=>300),
			array('field_key, value', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'field_key' => tm('Key'),
			'value' => tm('Value'),
			'title' => tm('Title'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('title',$this->title,true);
		$criteria->compare('field_key',$this->field_key,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static  function getCustomFieldByKey($key)
    {
       $lang = Yii::app()->language;
       $customField = CustomField::model()->lang($lang)->find( array(
           'condition'=>'field_key =:fkey',
           'params'=> array(':fkey'=>$key))
       );

       if(!empty($customField)) {
           return $customField->value;
       } else {
           $lang = Yii::app()->language;
           $customField = new CustomField();
           $customField->title = $key;
           $customField->value = $key;
           $customField->field_key = $key;
           $customField->lang = $lang;
           $customField->save();
       }
    }

    public function defaultScope() {
        return array(
            'condition' => "lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "lang='$lang'",
        ));
        return $this;
    }
}
