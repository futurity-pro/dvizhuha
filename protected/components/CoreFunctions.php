<?php

class CoreFunctions {

    public static function redirectTo($redirectsField, $redirectUrl) {
        if(!empty($redirectsField)) {
            $redirects = str_replace("\n\r", "\n", $redirectsField);
            $redirectTo = explode("\n", $redirects);

            foreach(array_keys($redirectTo) as $key) {
                $redirectTo[$key] = trim($redirectTo[$key]);
            }
            $currentUrl =  Yii::app()->request->requestUri;

            if(in_array($currentUrl, $redirectTo)){
                Yii::app()->request->redirect($redirectUrl);
            }
        }
    }

    public static function getAdminEmail()
    {
        $meta =  Meta::model()->findByPk(Yii::app()->name  . '(' . Yii::app()->language . ')');
        return (!empty($meta)) ? $meta->admin_email : false;
    }

}
