<div class="sidebar" id="sidebar">

    <?php $this->widget('cabinet-widgets.Nav', array(
        'type'=>'list',
        'items' => array(
            array(
                'label' => t('Content'),
                'icon' => 'globe',
                'url' => array('#'),
                'items' => array(

                    array(
                        'label' => t('Pages'),
                        'icon' => 'globe',
                        'url' => array('/pages/admin'),
                        'active'=> $this->module->getName() == 'pages' AND Yii::app()->controller->action->id != 'meta'
                    ),

                    array(
                        'label' => t('Meta'),
                        'icon' => 'icon-file-alt',
                        'url' => array('/pages/admin/meta/'),
                        'active'=> $this->module->getName() == 'pages' AND Yii::app()->controller->action->id == 'meta'
                    ),

                    array(
                        'label' => t('Menu'),
                        'icon' => 'icon-list',
                        'url' => array('/menumanager/'),
                        'active'=> $this->module->getName() == 'menumanager'
                    ),
                    array(
                        'label' => t('Portfolio'),
                        'icon' => 'icon-picture',
                        'url' => array('/portfolio/'),
                        'active'=> $this->module->getName() == 'portfolio'
                    ),
                    array(
                        'label' => t('Blog'),
                        'icon' => 'icon-picture',
                        'url' => array('/blog/default/admin'),
                        'active'=> $this->module->getName() == 'blog'
                    ),

                    array(
                        'label' => t('Reviews'),
                        'icon' => 'icon-user',
                        'url' => array('/review/'),
                        'active'=> $this->module->getName() == 'review'
                    ),
                    array(
                        'label' => t('Advantages'),
                        'icon' => 'icon-trophy',
                        'url' => array('/advantage/'),
                        'active'=> $this->module->getName() == 'advantage'
                    ),
                    array(
                        'label' => t('Custom Fields'),
                        'icon' => 'icon-tag',
                        'url' => array('/customfields/'),
                        'active'=> $this->module->getName() == 'customfields'
                    ),
                )
            ),
        )
    )); ?>
    <div class="copyright">
        <?php echo t('Control Panel') ?> <br>
        <a target="_blank" href="http://cms.futurity.pro/">FuturityCMS</a>
    </div>
</div>
