Движуха
=======

Настройка и установка проекта
* Создайте базу данных и имортируйте backup/db.sql в созданную базу
* Скопируйте файл protected/config/data/dp.php.dist в protected/config/data/dp.php и настройте подключение
* Скопируйте файл .htaccess.dist в .htaccess и замените строку SetEnv APPLICATION_ENV dev на SetEnv APPLICATION_ENV prod
* Поставьте права на запись следующим папкам assets, protected/assets, uploads, protected/runtime
* Обновите ядро yii, если необходимо с помощью комманды git submodule intit && git submodule update
