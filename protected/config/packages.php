<?php

return array(
	'site' => array(
		'baseUrl'=> '/site/',
		'css'=>array(
			'css/bootstrap.css',
			'css/bootstrap-checkbox.css',
			'css/bootstrap-theme.css',
			'css/fonts.css',
		),
		'js'=>array(
			'js/bootstrap.min.js',
			'js/jquery.placeholder.js',
			'js/custom.js',
		),
		'depends'=>array('jquery')
	),
    'ace-theme' => array(
		'baseUrl'=> '/themes/ace/assets/',
		'js'=>array(
			'js/jquery-ui-1.10.3.custom.min.js',
			'js/jquery.ui.touch-punch.min.js',
			'js/jquery.slimscroll.min.js',
			'js/jquery.easy-pie-chart.min.js',
			'js/jquery.sparkline.min.js',
			'js/flot/jquery.flot.min.js',
			'js/flot/jquery.flot.pie.min.js',
			'js/flot/jquery.flot.resize.min.js',
			'js/date-time/bootstrap-datepicker.min.js',
			'js/moment.min.js',
			'js/markdown/markdown.min.js',
			'js/markdown/bootstrap-markdown.min.js',
			'js/jquery.hotkeys.min.js',
			'js/bootstrap-wysiwyg.min.js',
			'js/bootbox.min.js',
			'js/ace-elements.min.js',
			'js/ace.min.js',
		),
		'depends'=>array('jquery')
	),



);
