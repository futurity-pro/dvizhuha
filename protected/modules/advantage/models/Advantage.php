<?php

class Advantage extends CActiveRecord
{
    private  static $AdvantagePhotosUpload = '/uploads/advantage/';

    public function tableName()
    {
        return 'advantages';
    }

    public function rules()
    {
        return array(
        array('title, text', 'required'),
        array('title, image', 'length', 'max' => 100),
        array('url', 'length', 'max' => 200),
        array('visible, created_at, lang', 'safe'),
        array('title, text', 'safe', 'on'=>'search'),
    );
    }

	public function attributeLabels()
	{
        return array(
            'created_at' => tm('Created'),
            'title'      => tm('Title'),
            'text'       => tm('Text'),
            'image'      => tm('Image'),
            'visible'    => tm('Visible'),
        '   Click here to choose file' => tm('Click here to choose file')
        );
	}

    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('title',$this->title,true);
        $criteria->compare('text',$this->text,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getImagePath()
    {
        return Advantage::getUploadDir() . $this->image;
    }

    public static function getUploadDir($withoutFirstSlash = false)
    {
        if($withoutFirstSlash == true) {
             return substr(self::$AdvantagePhotosUpload, 1);
        }
        return self::$AdvantagePhotosUpload;
    }

    public static function removeImage($filename)
    {
        $path = self::getUploadDir(true) . $filename;
        if (file_exists($path)){
            return @unlink($path);
        } else {
            throw new CHttpException(404, "$path file doesn't exists");
        }
    }

    public static function getModels()
    {
        $lang = Yii::app()->language;
        return Advantage::model()->lang($lang)->findall(array('condition' => 'visible = 1'));
    }

    public function defaultScope() {
        return array(
            'condition' => "lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(
            array(
            'condition' => "lang='$lang'",
            )
        );
        return $this;
    }

}
