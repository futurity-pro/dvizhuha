<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <?php Yii::app()->bootstrap->register(); ?>

    <!--basic styles-->

    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-responsive.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/font-awesome.min.css"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->

    <!--page specific plugin styles-->

    <!--fonts-->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-fonts.css"/>
    <!--ace styles-->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/datepicker.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-skins.min.css"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-ie.min.css"/>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/styles.css"/>
    <% Yii::app()->clientScript->registerPackage('ace-theme'); %>

</head>

<body>

<% $this->widget('AdminPanel'); %>

<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
    <% $this->renderPartial('//layouts/blocks/tree-sidebar')%>

<div class="main-content">
<!--div class="breadcrumbs" id="breadcrumbs">
    <?//php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
//        'htmlOptions' => array('class'=>''),
//        'homeLink' => CHtml::link(t('Control Panel'), Yii::app()->homeUrl),
//        'links' => $this->breadcrumbs,
//        'separator' => '<i class="icon-angle-right arrow-icon"></i>'
    //)); ?>

<div class="nav-search" id="nav-search"></div>
</div-->

<div class="page-content">
<?php echo $content; ?>
<div class="row-fluid">
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>


    <div class="clear"></div>
</div>
</div>
<!--/#ace-settings-container-->
</div>
<!--/.main-content-->
</div>
<!--/.main-container-->

<!--basic scripts-->


<!--page specific plugin scripts-->

<!--[if lte IE 8]>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
    $(function(){

        function showErrorAlert (reason, detail) {
            var msg='';
            if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
            else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
        }

        $('#editor').ace_wysiwyg();
    });
</script>
<!-- -->

<script type="text/javascript">
    $(function () {
        $('.submenu .active').parent().parent().addClass('active open');
    })
</script>

</body>
</html>