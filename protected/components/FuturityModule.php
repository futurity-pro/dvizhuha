<?php
class FuturityModule extends CWebModule {

	public $defaultController = 'default';

	public function init()
    {
        $this->defineAlias();
        $this->basicImports();

	}

    public static function defineAlias()
    {
        Yii::setPathOfAlias('form', 'application.modules.blocks.form');
        Yii::setPathOfAlias('fields', 'application.modules.blocks.fields');
        Yii::setPathOfAlias('layouts', 'application.modules.blocks.layouts');
        Yii::setPathOfAlias('page-view', 'application.modules.pages.views.default.view');

    }
    public function  basicImports()
    {
        $this->setImport(array(
             $this->getName() . '.models.*',
             $this->getName() . '.components.*',
             $this->getName() . '.controllers.*',
            'application.components.CoreController',
        ));
        $this->setViewPath(Yii::app()->getBasePath() . '/modules/' . $this->getName(). '/views');
    }

    public static function pathToBlock($path)
    {
        list($alias, $block) = explode('::', $path);
        if (!empty($alias) AND !empty($block)) {
            $pathToBlock = Yii::getPathOfAlias($alias) . '/' .$block;
            return $pathToBlock;
        } else {
            throw new CHttpException(500, 'Wrong block path');
        }
    }
}
