<?php
class DefaultController extends CoreController
{
    public $imageWidth = 220;
    public $imageHeight = 205;
    public $baseModel = 'Portfolio';
    public $crudFormID = 'portfolio-form';
    public $multiLang = true;
}