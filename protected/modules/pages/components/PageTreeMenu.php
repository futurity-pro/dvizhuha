<?php

class PageTreeMenu extends CWidget {

	public function run()
	{

		$pages = Page::model()->findAll();
		$level = 0;
		//echo CHtml::tag('h3', array(), tm('PagesModule.core', 'Site Map'));

        echo CHtml::openTag('ul', array('class' => 'nav nav-list')) . "\n";
        echo CHtml::openTag('li', array('class'=>'open active'));
        //echo CHtml::link(  t('Pages') , array('/pages/admin'), array('class'=>'') );

		foreach ($pages as $n => $page)	{
			if ($page->level == $level) {
				echo CHtml::closeTag('li') . "\n";
            }
			else if ($page->level > $level) {
                echo CHtml::openTag('ul', array('class' => 'submenu')) . "\n";
            }
			else {
				echo CHtml::closeTag('li') . "\n";

				for ($i = $level - $page->level; $i; $i--)	{
					echo CHtml::closeTag('ul') . "\n";
					echo CHtml::closeTag('li') . "\n";
				}
			}
            $currentPageID = Yii::app()->getRequest()->getParam('id');
            if ($currentPageID == $page->id) {
			    echo CHtml::openTag('li', array('class'=>'active open'));
            } else {
                echo CHtml::openTag('li', array('class'=>'open'));
            }
            if($page->type == Page::TYPE_PAGE) {
                echo CHtml::link(  $page->page_title , array('/pages/admin/update', 'id' => $page->id));
            } else {
                echo CHtml::link(  $page->page_title , array('/pages/admin/update', 'id' => $page->id), array('class'=>'dropdown-toggle') );
            }
			$level = $page->level;
		}

		for ($i = $level; $i; $i--)	{
			echo CHtml::closeTag('li') . "\n";
			echo CHtml::closeTag('ul') . "\n";
		}
        echo CHtml::closeTag('li') . "\n";
        echo CHtml::closeTag('ul') . "\n";
	}

}