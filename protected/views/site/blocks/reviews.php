<?php
/**
 * @file reviews.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 17.02.14 17:18
 */
?>

<div class="container container-marg">
    <div class="container-bg">
        <h2 class="container-h2"><% echo CustomField::getCustomFieldByKey('clientsReviewTitle')%></h2>
        <div class="reviews">
            <% $itemCounter = 0;%>
            <% foreach(Review::getModels() as $review) :%>
                <% $itemCounter++; %>
                <div class="col-sm-4">
                    <div class="reviews-col reviews-col-<% echo $itemCounter%>">
                        <img src="<% echo $review->getImagePath(); %>" class="img-responsive" alt="">
                        <%if(!empty($review->text)):%>
                            <p><% echo $review->text%> </p>
                        <% endif; %>
                        <%if(!empty($review->author)):%>
                            <p class="reviews-name"><% echo $review->author%> </p>
                        <% endif; %>
                    </div>
                </div>
                <% if($itemCounter == 3) $itemCounter = 0; %>
            <% endforeach; %>
        </div>
    </div>
</div>