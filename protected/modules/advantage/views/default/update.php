<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Advantage'), array('/advantage'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo tm('Edit Item')%>
        </small>
    </h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>