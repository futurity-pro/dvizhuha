<?php

/**
 * class ajEMailer
 *
 * Depends on:
 *  - EMailer
 *  - PHPMailer
 *
 * @author  Stanislav Terleckiy <terleckiy@zfort.com>
 * @version 1.0
 * @package AJ
 */
abstract class ajEMailer
{
    /**
     *  send
     *  Do send one email
     * @example
     * <pre>
        ajEMailer::send(
            array(
                'to' => array(
                    'recipient_one@example.com'  =>'Recipient One',
                    'recipient_two@example.com'  =>'Recipient Two',
                ),
                'cc' => array(
                    'recipient_CC@example.com'  =>'Recipient CC',
                ),
                'bcc' => array(
                    'recipient_BC@example.com'  =>'Recipient BC',
                ),
                'ReplyTo' => array(
                    'alljobs@alljobs.co.uk'=>'AllJobs Team',
                ),
                'files' => array(
                    'SomeFile.doc'=>'/path/to/some/file/SomeFile.doc',
                ),
                'subject'=>'This message just a test, please, do not reply!',
                'view'   => 'invoice',
                'body'   => 'Some text in body.',
                'data'   => array(
                    'message'=>'This message just a test, please, do not reply!',
                ),
            )
        );
     * </pre>
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @param  array $message prepared message
     * @param  bool  $dev if true, output email content, instead send
     */
    public static function send($message = false, $dev=false)
    {
        if (false === $message)
        {
            return false;
        }
        if (is_array($message))
        {
            return self::sendOne($message, $dev);
        }
    }

    /**
     *  sendMany
     *  Do send emails
     *
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @param  array $messages array of message with parameters
     * @param  bool  $cli if true run as ConsoleCommand
     * @param  bool  $dev if true, output email content, instead send
     */
    public static function sendMany($messages = false, $dev=false)
    {
        if (false === $messages)
        {
            return false;
        }
        if (is_array($messages))
        {
            return self::sendEach($messages, $dev);
        }
    }

    /**
     * sendEach
     *
     * Send each email message
     *
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @param  array $messages
     * @param  bool  $dev if true, output email content, instead send
     */
    protected static function sendEach($messages, $dev=false)
    {
        foreach ($messages as $message)
        {
            self::sendOne($message, $dev);
        }
        return true;
    }

    /**
     * sendOne
     * Do send one message
     *
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @param  array $message
     * @param  bool  $dev if true, output email content, instead send
     */
    protected static function sendOne($message, $dev=false)
    {
        if (false === isset($message['to']) || 
            (  false === isset($message['view'])
            && false === isset($message['data'])
            && false === isset($message['body'])))
        {
            throw new Exception('Message not valid.' . PHP_EOL . 'Min. correct message: array(\'to\'=>\'example@example.com\', \'body\' => \'Some message\')');
        }
        //set default sender Email address
        Yii::app()->mailer->IsHTML(true);
        //set default sender Email address
        Yii::app()->mailer->From = Yii::app()->params['mail']['fromEmail'];
        //set default sender Name
        Yii::app()->mailer->FromName = Yii::app()->params['mail']['fromName'];
        //set subject
        if (isset($message['subject']))
        {
            Yii::app()->mailer->Subject = $message['subject'];
        }
        //set message body
        if (isset($message['data']) && isset($message['view']))
        {
            Yii::app()->mailer->getView($message['view'], $message['data'], self::getLayout($message['view']));
        }
        else if (isset($message['body']))
        {
            Yii::app()->mailer->Body = $message['body'];
        }
        //add all attached files from $message['files']
        if (isset($message['files']))
        {
            foreach ((array)$message['files'] as $name => $path)
            {
                Yii::app()->mailer->AddAttachment($path, is_int($name) ? basename($path) : $name);
            }
        }
        Yii::app()->mailer->clearAddresses();
        //add all TO recipient addresses from $message['to']
        foreach ((array)$message['to'] as $address => $name)
        {
            Yii::app()->mailer->AddAddress(is_int($address) ? $name : $address, is_int($address) ? '' : $name);
        }
        //add all CC recipient addresses from $message['cc']
        if (isset($message['cc']))
        {
            foreach ((array)$message['cc'] as $address => $name)
            {
                Yii::app()->mailer->AddCC(is_int($address) ? $name : $address, is_int($address) ? '' : $name);
            }
        }
        //add all BCC recipient addresses from $message['bcc']
        if (isset($message['bcc']))
        {
            foreach ((array)$message['bcc'] as $address => $name)
            {
                Yii::app()->mailer->AddBCC(is_int($address) ? $name : $address, is_int($address) ? '' : $name);
            }
        }
        //add all recipient addresses from $message['ReplyTo']
        if (isset($message['ReplyTo']))
        {
            foreach ((array)$message['ReplyTo'] as $address => $name)
            {
                Yii::app()->mailer->AddReplyTo(is_int($address) ? $name : $address, is_int($address) ? '' : $name);
            }
        }
        if ($dev)
        {
            self::printMessage($message);
        }
        //try to send the email
        try
        {
            return Yii::app()->mailer->Send();
        }
        catch (phpmailerException $e)
        {
            //do something with catched exeption
        }
	    return false;
    }

    /**
     * getLayout
     * Gets layout by view name
     *
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @param  string $view 
     * @return string layout name if is set in Yii::app()->params['mail']['layouts'], null otherwise
     */
    protected static function getLayout($view)
    {
        $layouts = Yii::app()->params['mail']['layouts'];

        if (isset($layouts[$view]))
        {
            return $layouts[$view];
        }
        return null;
    }
    /**
     * printMessage
     * print prepared message
     *
     * @author Stanislav Terleckiy <terleckiy@zfort.com>
     * @return string
     */
    protected static function printMessage($message)
    {
        echo Yii::app()->mailer->Body;
    }
}