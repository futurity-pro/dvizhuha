<?php
class Meta extends CActiveRecord
{
    public $robotsTxt;

	public function tableName()
	{
		return 'meta';
	}

	public function rules()
	{

		return array(
			array('title, meta_template_title', 'length', 'max' => 100),
			array('description, keywords', 'length', 'max' => 500),

            #custom code on meta section
			array('cm_head ,cm_after_open_body , cm_before_close_body, cm_footer', 'safe'),
			array('meta_template_keywords , meta_template_description, redirects', 'safe'),
            array('admin_email', 'email'),
			array('title, description, keywords', 'safe', 'on'=>'search'),
            array('robotsTxt','filter','filter' => array($obj = new CHtmlPurifier(),'purify') ),
		);
	}



    public function attributeLabels()
    {
        return array(
            'title'                     => t('Title'),
            'description'               => t('Description'),
            'keywords'                  => t('Keywords'),
            'cm_head'                   => t ('in')  . ' '  . t('head'),
            'cm_after_open_body'        => t ('after')  . ' '  .  '&lt;body&gt;',
            'cm_before_close_body'      => t('before') . ' '  . '&lt;body/&gt;',
            'cm_footer'                 => t('in') . ' '  . 'footer',
            'meta_template_title'       => t('Meta') . ' '  . t('Template') . ' ' . t('Title'),
            'meta_template_description' => t('Meta') . ' '  . t('Template') . ' ' . t('Description'),
            'meta_template_keywords'    => t('Meta') . ' '  . t('Template') . ' ' . t('Keywords'),
            'robotsTxt'                 => t('File robots.txt'),
            'redirects'                 => tm('Redirects'),
            'admin_email'               => tm('Admin Email'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('title', $this->title,true);
        $criteria->compare('description', $this->description,true);
        $criteria->compare('keywords', $this->keywords, true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function beforeSave()
    {
        if(parent::beforeSave()) {
            $this->writeRobotsTxt();
            return true;
        }
        else {
            return false;
        }
    }

    public function getMetaTitle($title = '')
    {
        $metaTemplate =  $this->meta_template_title;
        $metaTitle = (!empty($title)) ? $title : $this->title;
        $result = str_replace( '{page-title}', $metaTitle, $metaTemplate);
        return !empty($result) ? $result : $this->title;
    }

    public function getMetaDescription($description = '')
    {
        $metaTemplate =  $this->meta_template_description;
        $description = (!empty($description)) ? $description : $this->description;
        $result = str_replace( '{page-description}', $description, $metaTemplate);
        return !empty($result) ? $result : $this->description;
    }

    public function getMetaKeywords($keywords = '')
    {
        $metaTemplate =  $this->meta_template_keywords;
        $metaKeywords = (!empty($keywords)) ? $keywords :  $this->keywords;
        $result = str_replace( '{page-keywords}', $metaKeywords, $metaTemplate);
        return !empty($result) ? $result : $this->keywords;
    }



    public function afterFind()
    {
        $this->robotsTxt = $this->readRobotsTxt();
    }

    public function writeRobotsTxt()
    {
        file_put_contents(self::getRobotTxtFilePath(), $this->robotsTxt);
    }

    public function  readRobotsTxt() {
        return file_get_contents(self::getRobotTxtFilePath());
    }

    public static function getRobotTxtFilePath()
    {
        return Yii::getPathOfAlias('webroot') . '/' . 'robots.txt';
    }

    public function defaultScope() {
        return array(
            'condition' => "lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "lang='$lang'",
        ));
        return $this;
    }
}
