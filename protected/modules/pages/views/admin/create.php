<%
$this->pageTitle = 'Создание страницы';
$this->breadcrumbs = array(
	'Статические страницы' => array('index'),
	$this->pageTitle,
);
%>
<div class="page-header position-relative">
    <h1>
        <% echo t('Create Item'); %>
    </h1>
</div>

<% echo $this->renderPartial('_form', array('model' => $model)) %>