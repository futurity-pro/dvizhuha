<?php
/**
 * @file LanguageSelector.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 18.02.14 14:29
 */
class AdminPanel extends CWidget
{
    public function run()
    {
        if(!Yii::app()->user->isGuest) {
            $baseUrl = Yii::app()->baseUrl;
            $cs = Yii::app()->getClientScript();
            $cs->registerScriptFile($baseUrl.'/protected/components/widgets/js/admin-panel.js');
            $cs->registerCssFile($baseUrl.'/protected/components/widgets/css/admin-panel.css');
            $isBackend = (!empty(Yii::app()->controller->backend));
            $this->render('adminPanel', compact('isBackend'));
        }
    }
}
