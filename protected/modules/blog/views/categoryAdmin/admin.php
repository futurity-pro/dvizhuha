<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Blog'), array('/blog/default/admin'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo tm('Manage Category')%>
        </small>
    </h1>
</div>


<a href="<% echo $this->createUrl('/blog/categoryAdmin/create') %>"
   class="btn btn-app btn-primary no-radius">
    <i class="icon-file bigger-230"></i>
    <% echo t('Create') %>
</a>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),

    'summaryText'=>'',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'columns'=>array(
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => '$data->name',
            'filter' => false,
            'sortable' => false,
        ),

        array(
            'class'=>'CButtonColumn',
            //'class'=>'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => t('Are you sure you want to delete this item?'),
            'template'=>'{update}{delete}',
            'htmlOptions' => array('class'=>'infopages_buttons_column'),
            'buttons' => array(
                'update' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-edit'),

                ),
                'delete' => array(
                    'label'=>'',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-remove' ),

                ),
            ),
        ),
    ),
)); ?>
