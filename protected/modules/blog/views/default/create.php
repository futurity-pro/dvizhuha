<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Blog'), array('/blog'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo t('Create Item')%>
        </small>
    </h1>
</div>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>