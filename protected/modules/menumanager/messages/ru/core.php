<?php
/**
 * @file core.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 12.02.14 21:09
 */
return array(
    'Menu Manager' => 'Менеджер меню',
    'Link' => 'Ссылка меню',
    'Title' => 'Заголовок меню',
);