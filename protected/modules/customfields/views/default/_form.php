<% $this->beginContent(FuturityModule::pathToBlock('layouts::form')); %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'title',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'field_key',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'value',
        ));
    %>


    <% $this->renderPartial( FuturityModule::pathToBlock('form::form-actions'), array('model' => $model)); %>

<% $this->endContent(); %>


