<% $this->widget('AdminPanel'); %>
<!-- Header -->
<header class="header">
    <div class="container">
        <a href="/" class="header-logo"></a>
        <div class="row header-row-pad">
            <div class="col-md-12">
                <p class="navbar-phone"><% echo CustomField::getCustomFieldByKey('phoneOnHeader')%></p>
                <p class="navbar-mail"><% echo CustomField::getCustomFieldByKey('emailOnHeader')%></p>
                <% $this->widget('application.components.widgets.LanguageSelector');  %>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Меню</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <? $menuItemCounter = 0;?>
                                <% foreach(Menu::getMenuByKey('header') as $menuItem ):%>

                                    <li><a href="<% echo $menuItem->url %>"><% echo $menuItem->title %></a></li>

                                    <%if ($menuItemCounter == 2):%>
                                        <li class="empty"></li>
                                    <% endif%>
                                    <% $menuItemCounter++; %>
                                <% endforeach;%>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>

</header>
<!-- Header -->