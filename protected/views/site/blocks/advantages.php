<div class="container">
    <div class="container-bg">
        <h2 class="container-h2"><% echo CustomField::getCustomFieldByKey('ownStrongSides')%></h2>
        <div class="advantages">
            <% $advantageNumber = 1; %>
            <% foreach(Advantage::getModels() as $advantage): %>
            <div class="advantages-col advantages-<%echo $advantageNumber%>">
                <div class="advantages-col-in">
                    <a href="<% echo $advantage->url %>">
                        <img src="<% echo $advantage->getImagePath() %>" class="img-responsive" alt="">
                        <span class="advantages-bot">
                            <span class="advantages-txt"><% echo $advantage->title %></span>
                            <span class="advantages-shadow img-responsive"></span>
                        </span>
                    </a>
                    <% if(!empty($advantage->text)) :%>
                        <p><% echo $advantage->text %></p>
                    <% endif;%>
                </div>
            </div>

            <% $advantageNumber++; %>
            <% if($advantageNumber  == 4): %>
                <% $advantageNumber = 1; %>
            <% endif; %>

            <% endforeach; %>
        </div>
    </div>
</div>