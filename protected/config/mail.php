<?php
return array(
    'Mailer'      => 'smtp',
    'ContentType' => 'text/html',
    'Host'        => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_host.php',
    'Port'        => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_port.php',
    'SMTPAuth'    => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_auth.php',
    'SMTPSecure'  => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_secure.php',
    'Username'    => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_user.php',
    'Password'    => require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'mail_pass.php',
    'class'       => 'application.extensions.mailer.EMailer',
    'pathViews'   => 'application.views.mail',
    #'pathLayouts' => 'webroot.themes.ud.views.emails.layouts'
);