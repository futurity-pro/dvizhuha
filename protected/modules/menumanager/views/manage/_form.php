
<div class="form-horizontal">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'menu-form',
        'enableAjaxValidation' => true,
    )); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model,'name', array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
            <?php echo $form->error($model,'name', array('class'=>'text-error')); ?>
        </div>
    </div>


    <div class="form-actions">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            <% echo ($model->isNewRecord) ? t('Create') : t('Save'); %>
        </button>
        &nbsp; &nbsp; &nbsp;
        <button class="btn">
            <a href="<% echo $this->createUrl('/' . getCurrentModuleName()) %>">
                <i class="icon-remove bigger-110"></i>
                <% echo t('Cancel')%>
            </a>
        </button>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->


