<?php
/**
 * @file works.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 17.02.14 17:17
 */
?>
<div class="container container-marg">
    <div class="container-bg">
        <h2 class="container-h2"><% echo CustomField::getCustomFieldByKey('ownWorksTitle')%></h2>
        <div class="works">
            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">
                    <% $itemNumber = 1; %>
                    <% $itemQuarterCounter = 0; %>
                    <% foreach(Portfolio::getModels() as $item) : %>
                        <% $itemQuarterCounter ++; %>
                        <% if($itemQuarterCounter == 1): %>
                        <div class="item">
                        <% endif; %>
                            <div class="col-sm-3 works-col-<% echo $itemNumber %>">
                                <a href="<% echo $item->url %>">
                                    <img src="<% echo $item->getImagePath() %>"  class="img-responsive">
                                    <span class="works-bot">
                                        <span class="works-txt"><% echo $item->title %></span>
                                        <span class="works-shadow img-responsive"></span>
                                    </span>
                                </a>
                            </div>
                        <% if($itemQuarterCounter  == 4): %>
                        </div>
                        <% $itemQuarterCounter = 0; %>
                        <% endif; %>

                        <% $itemNumber++; %>
                        <% if($itemNumber  == 4): %>
                            <% $itemNumber = 1; %>
                        <% endif; %>

                    <% endforeach; %>
                </div>
            </div>
            <!--/myCarousel-->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
        </div>
    </div>
</div>