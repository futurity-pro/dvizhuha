<?php

class m140224_075512_add_redirect_field_to_pages extends CDbMigration
{
    public function safeUp() {
        $this->addColumn('pages', 'redirects', 'text NULL');
        $this->addColumn('meta', 'redirects', 'text NULL');
    }

    public function safeDown() {
        $this->dropColumn('pages', 'redirects');
        $this->dropColumn('meta', 'redirects');
    }
}