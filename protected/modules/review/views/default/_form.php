<% $this->beginContent(FuturityModule::pathToBlock('layouts::form')); %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'author',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-area'),
        array(
            'model' => $model,
            'name' => 'text',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::image-upload'),
        array(
            'model' => $model,
            'name' => 'image',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('form::form-actions'), array('model' => $model)); %>

<% $this->endContent(); %>


