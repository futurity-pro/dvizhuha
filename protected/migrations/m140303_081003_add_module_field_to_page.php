<?php

class m140303_081003_add_module_field_to_page extends CDbMigration
{
    public function safeUp() {
        $this->addColumn('pages', 'module', 'varchar(50) NULL');
    }

    public function safeDown() {
        $this->dropColumn('pages', 'module');
    }
}