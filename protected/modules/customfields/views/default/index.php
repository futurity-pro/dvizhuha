<?php
/* @var $this CustomFieldsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Custom Fields',
);

$this->menu=array(
	array('label'=>'Create CustomFields', 'url'=>array('create')),
	array('label'=>'Manage CustomFields', 'url'=>array('admin')),
);
?>

<h1>Custom Fields</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
