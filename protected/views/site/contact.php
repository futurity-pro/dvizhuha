<div class="homepage homepage-bg">
    <div class="container">
        <div class="container-bg">
            <div class="internal">
                <% if(Yii::app()->user->hasFlash('contact')): %>

                <div class="flash-success">
                    <% echo Yii::app()->user->getFlash('contact'); %>
                </div>

                <% else: %>
                <h1><% echo CustomField::getCustomFieldByKey('contactWithUsTitle')%></h1>
                <p>
                    <% echo CustomField::getCustomFieldByKey('contactWithUsText')%>
                </p>

                <% $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); %>
                    <div class="subscribe">
                        <br>
                            <?php echo $form->errorSummary($model); ?>
                            <% echo $form->textField($model,'name', array('placeholder'=> CustomField::getCustomFieldByKey('contactFormName') , 'class'=>'form-control')); %>
                            <% echo $form->textField($model,'email', array('placeholder'=> CustomField::getCustomFieldByKey('contactFormEmail'), 'class'=>'form-control')); %>
                            <% echo $form->textField($model,'subject', array('placeholder'=> CustomField::getCustomFieldByKey('contactFormTitle'), 'class'=>'form-control')); %>
                            <% echo $form->textField($model,'phone', array('placeholder'=>CustomField::getCustomFieldByKey('contactFormPhone'), 'class'=>'form-control')); %>
                            <% echo $form->textArea($model,'message', array('placeholder'=> CustomField::getCustomFieldByKey('contactFormMessage'), 'class'=>'form-control')); %>
                        <p>* — <% echo CustomField::getCustomFieldByKey('fieldRequiredForFillingText')%></p>
                        <br/>
                    </div>
                <% echo CHtml::submitButton( t('Submit'), array('class'=>'btns')); %>
                <% $this->endWidget(); %>
                <br/>
                <% endif; %>
            </div>
        </div>
    </div>
</div>




