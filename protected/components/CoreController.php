<?php
class CoreController extends Controller
{
    public $backend = true;

    public  $layout = 'webroot.themes.ace.views.layouts.main';
    public  $defaultAction = 'admin';
    public  $baseModel;
    public  $crudFormID;

    public  $multiLang = false;

    public $imageWidth;
    public $imageHeight;

    public function init()
    {
        Yii::app()->setTheme('ace');
    }

    public function filters()
    {
        return array(
            'accessControl'
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'users'=> array('@')),
            array('deny',  'users' => array('*') ),
        );
    }

    public function actions()
    {
        return array(
            'order' => array(
                'class' => 'ext.yiiSortableModel.actions.AjaxSortingAction',
            ),
        );
    }

    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    public function actionCreate($id = null)
    {
        $model = new $this->baseModel;

        if ( $this->multiLang) {
            $model->lang =  Yii::app()->language;
        }
        $this->performAjaxValidation($model);

        if ( isset($_POST[$this->baseModel]) ) {
            $model->attributes=$_POST[$this->baseModel];
            if ($model->save()){
                $this->redirect(array('admin'));
            }
        }
        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);

        if ( isset($_POST[$this->baseModel]) ) {
            $model->attributes = $_POST[$this->baseModel];
            if ($model->save()) {
                $this->redirect(array('admin'));
            }
        }
        $this->render('update', array('model' => $model));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $returnUrl = $_POST['returnUrl'];
            $this->redirect(isset($returnUrl) ? $returnUrl : array('admin'));
        }
    }

    public function loadModel($id)
    {
        $baseModel = new $this->baseModel();
        $model = $baseModel::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
        return $model;
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider($this->baseModel);
        $this->render('index',array('dataProvider' => $dataProvider));
    }

    public function actionAdmin($id = null)
    {
        $model = new $this->baseModel('search');
        $model->unsetAttributes();
        if ( isset($_GET[$this->baseModel]) ) {
            $model->attributes = $_GET[$this->baseModel];
        }

        $this->render('admin', array('model'=>$model));
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $this->crudFormID) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionChangeImage($id)
    {
        if (Yii::app()->request->isAjaxRequest AND !empty($_POST['filename'])) {
            $filename = $_POST['filename'];
            $baseModel  = new $this->baseModel;
            $item = $baseModel::model()->findByPk($id);

            if (!empty($item)) {
                $item->image = $filename;
                $item->save(false);
            } else {
                throw new CHttpException(404, 'this item does not exist.');
            }
        } else {
            throw new CHttpException(500, 'Bad request');
        }
    }

    public function actionUploadImage()
    {
        if (Yii::app()->request->isAjaxRequest) {
            Yii::import("ext.EAjaxUpload.qqFileUploader");
            $baseModel  = new $this->baseModel;

            $folder = dirname(Yii::getPathOfAlias('application')) . $baseModel::getUploadDir();
            $allowedExtensions = array("jpg", "jpeg", "gif", "png");
            $sizeLimit = 10 * 1024 * 1024;
            $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
            $result = $uploader->handleUpload($folder, true);
            if (!empty($result['filename'])) {
                $path = $folder . DIRECTORY_SEPARATOR . $result['filename'];
                imageResize($path, $path, $this->imageWidth, $this->imageHeight);

                $result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;
            } else {
                $uploadDir = $baseModel::getUploadDir();
                throw new CHttpException(500, "Error file uploading, check $uploadDir exist and permission is ok");
            }
        }
    }

    public function actionCancelUpload($id)
    {
        if (Yii::app()->request->isAjaxRequest AND !empty($id)) {
            $model = $this->loadModel($id);
            if(!empty($model)) {
                $filename = $model->image;
                $baseModel = new $this->baseModel;
                if ( $baseModel::removeImage($filename)) {
                    $model->image  = null;
                    $model->save();
                }
            } else {
                throw new CHttpException(404, 'This item doesn\'t exists');
            }
        } else {
            throw new CHttpException(500, 'Bad request');
        }
    }

    public function actionChangeVisibility($id)
    {
        if (Yii::app()->request->isAjaxRequest AND !empty($id)) {
            $model = $this->loadModel($id);
            $model->visible = ($model->visible == 1 ? 0 : 1);
            $model->save();
        } else {
            throw new CHttpException(500, 'Bad request');
        }
    }
    public function createDefaultModuleAction($action)
    {
        return '/' . getCurrentModuleName() . '/'. $this->id . '/' . $action ;
    }

}