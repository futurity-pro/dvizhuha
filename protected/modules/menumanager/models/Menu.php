<?php

class Menu extends CActiveRecord
{

	public function tableName()
	{
		return 'menu';
	}


	public function rules()
	{
		return array(
			array('id, name, lang, menu_key', 'required'),
			array('id', 'length', 'max'=>10),
			array('name, menu_key', 'length', 'max'=>100),
			array('lang', 'length', 'max'=>2),

			array('id, name, lang', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array('menuItems' => array(
            self::HAS_MANY, 'MenuItem', 'menu_id',  'order'     => '`order`',   'condition' => 'visible = 1', 'alias'=>'mi'),

        );
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => t('Name'),
			'lang' => t('Lang'),
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		//$criteria->compare('menu_key',$this->menu_key,true);
		$criteria->compare('lang',$this->lang,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public  static function getMenuByKey($key)
    {
        $lang = Yii::app()->language;
        $menu = Menu::model()->lang($lang)->with('menuItems')->find(
            array(
                'alias' => 'm',
                'condition' => 'menu_key = :mk',
                'params'=> array(':mk' => $key)
            )
        );
        $menuExist = Menu::model()->lang($lang)->findByAttributes(array('menu_key'=>$key));
        if(!empty($menuExist)) {
            if(!empty($menu)) {
                $items = $menu->menuItems();
                return $menuItems =  !empty($items) ? $menu->menuItems : array();
            }
        } else {
            $menu = new Menu();
            $menu->menu_key = $key;
            $menu->name = $key;
            $menu->lang = $lang;
            $menu->save(false);
        }
        return array();
    }

    public function defaultScope() {

        return array(
            'condition' => "m.lang='" . Yii::app()->language . "'",
            'alias' => 'm'
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "m.lang='$lang'",
        ));
        return $this;
    }
}
