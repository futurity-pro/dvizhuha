<div class="page-header position-relative">
    <h1>
        <% echo tm('Manage Blog'); %>
    </h1>
</div>

<a href="<% echo $this->createUrl('/blog/default/create') %>" class="btn btn-app">
    <i class="icon-bullhorn bigger-230"></i>
    <% echo t('Post') %>
</a>

<a href="<% echo $this->createUrl('/blog/CategoryAdmin/admin') %>" class="btn btn-app btn-app">
    <i class="icon-edit bigger-230"></i>
    <% echo t('Category') %>
</a>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'summaryText'=>'',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'filter' => $model,
    'columns'=>array(
        array(
            'name'     => 'title',
            'type'     => 'raw',
            'value'    => '$data->title',
            'sortable' => true,
        ),
         array(
            'name'     => 'categorySearch',
            'type'     => 'raw',
            'value'    => '$data->category->name',
            'sortable' => true,
        ),

        array(
            'name'     => 'visible',
            'type'     => 'raw',
            'value'    => '$data->getVisible()',
            'filter'   => array(0 => t('No'), 1 => t('Yes')),
            'sortable' => true,
        ),


        array(
            'class'=>'CButtonColumn',
            'deleteConfirmation' => tm('Are you sure you want to delete this item?'),
            'template'=>'{update}{delete}',
            'htmlOptions' => array('class'=>'infopages_buttons_column'),
            'buttons' => array(
                'update' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-edit'),

                ),
                'delete' => array(
                    'label'=>'',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-remove' ),

                ),
            ),
        ),
    ),
)); ?>
