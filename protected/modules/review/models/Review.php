<?php

class Review extends CActiveRecord
{
    private  static $ImageUploadPath = '/uploads/review/';
	public function tableName()
	{
		return 'reviews';
	}

	public function rules()
	{
		return array(
			array('author, text', 'required'),
			array('image, author', 'length', 'max'=>100),
			array('visible, created_at', 'safe'),
			array('text', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'created_at' => tm('Created'),
			'title'      => tm('Title'),
			'text'       => tm('Text'),
			'image'      => tm('Image'),
			'visible'    => tm('Visible'),
			'author'     => tm('Author'),
			'Click here to choose file' => tm('Click here to choose file')
		);
	}

	public function search()
	{
    	$criteria=new CDbCriteria;
		$criteria->compare('author',$this->author,true);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getImagePath()
    {
        return Review::getUploadDir() . $this->image;
    }
    public static function getUploadDir($withoutFirstSlash = false)
    {
        if($withoutFirstSlash == true) {
             return substr(self::$ImageUploadPath, 1);
        }
        return self::$ImageUploadPath;
    }

    public static function removeImage($filename)
    {
        $path = self::getUploadDir(true) . $filename;
        if (file_exists($path)){
            return @unlink($path);
        } else {
            throw new CHttpException(404, "$path file doesn't exists");
        }
    }

    public static function getModels()
    {
        $lang = Yii::app()->language;
        return Review::model()->lang($lang)->findall(array('condition' => 'visible = 1'));
    }

    public function defaultScope() {
        return array(
            'condition' => "lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "lang='$lang'",
        ));
        return $this;
    }

}
