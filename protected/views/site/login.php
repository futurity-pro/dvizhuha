<div class="widget-main">
    <h4 class="header blue lighter bigger">
        <i class="icon-coffee green"></i>
        Введите  логин и пароль
    </h4>

    <div class="space-6"></div>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    <fieldset>
        <label>
            <span class="block input-icon input-icon-right">
                <?php echo $form->textField($model,'username', array('class'=>'span12')); ?>
                <?php echo $form->error($model,'username'); ?>
                <!--input type="text" class="span12" placeholder="Логин" /-->
                <i class="icon-user"></i>
            </span>
        </label>

        <label>
            <span class="block input-icon input-icon-right">
                <?php echo $form->passwordField($model,'password', array('class'=>'span12')); ?>
                <?php echo $form->error($model,'password'); ?>
                <!--input type="password" class="span12" placeholder="Пароль" /-->
                <i class="icon-lock"></i>
            </span>
        </label>

        <div class="space"></div>

        <div class="clearfix">
            <label class="inline">
                <input type="checkbox" />
                <span class="lbl"> Запомнить меня</span>
            </label>

            <div>
            <%// echo CHtml::tag('i', array('class'=>'icon-key')) %>
            <?php echo CHtml::submitButton(
                t('Sign In') ,
                array('class'=>'width-35 pull-right btn btn-small btn-primary')
            ); ?>
            </div>
        </div>

        <div class="space-4"></div>
    </fieldset>
    <?php $this->endWidget(); ?>

    <!--div class="social-or-login center">
        <span class="bigger-110">Или войдите используя</span>
    </div>

    <div class="social-login center">
        <a class="btn btn-primary">
            <i class="icon-facebook"></i>
        </a>

        <a class="btn btn-info">
            <i class="icon-twitter"></i>
        </a>

        <a class="btn btn-danger">
            <i class="icon-google-plus"></i>
        </a>
    </div-->

</div><!--/widget-main-->