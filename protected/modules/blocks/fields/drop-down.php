<?php
/**
 * @file text-field.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 27.02.14 19:31
 */
?>
<div class="control-group">
    <% echo CHtml::activeLabel($model, $name , array('class' => 'control-label')); %>
    <div class="controls">
        <% echo CHtml::activeDropDownList($model, $name , $options); %>
    </div>
</div>