<?php

class Controller extends CController {

	public $layout = '//layouts/column1';

	public $breadcrumbs = array();

	public $metaTitle;
	public $metaDescription;
	public $metaKeywords;

    public $customCodeHead;
    public $customCodeAfterOpenBody;
    public $customCodeBeforeCloseBody;
    public $customCodeFooter;

    protected function beforeAction($action)
    {
        $meta = Meta::model()->findByPk( Yii::app()->name  . '(' . Yii::app()->language . ')');
        if(!empty($meta)) {
            /* Adding custom code to right place on the layout*/
            $this->customCodeHead =             $meta->cm_head;
            $this->customCodeAfterOpenBody =    $meta->cm_after_open_body;
            $this->customCodeBeforeCloseBody =  $meta->cm_before_close_body;
            $this->customCodeFooter =           $meta->cm_footer;
        } else {
            $meta = new Meta();
            $meta->id = Yii::app()->name  . '(' . Yii::app()->language . ')';
            $meta->lang = Yii::app()->language;
            $meta->save();
        }
        return parent::beforeAction($action);
    }

    public function __construct($id,$module=null){
        parent::__construct($id,$module);
        // If there is a post-request, redirect the application to the provided url of the selected language
        if(isset($_POST['language'])) {
            $lang = $_POST['language'];
            $MultilangReturnUrl = $_POST[$lang];
            $this->redirect($MultilangReturnUrl);
        }
        // Set the application language if provided by GET, session or cookie
        if(isset($_GET['language'])) {
            Yii::app()->language = $_GET['language'];
            Yii::app()->user->setState('language', $_GET['language']);
            $cookie = new CHttpCookie('language', $_GET['language']);
            $cookie->expire = time() + (60*60*24*365); // (1 year)
            Yii::app()->request->cookies['language'] = $cookie;
        }
        else if (Yii::app()->user->hasState('language'))
            Yii::app()->language = Yii::app()->user->getState('language');
        else if(isset(Yii::app()->request->cookies['language']))
            Yii::app()->language = Yii::app()->request->cookies['language']->value;
    }
    public function createMultilanguageReturnUrl($lang='en'){
        if (count($_GET)>0){
            $arr = $_GET;
            $arr['language']= $lang;
        }
        else
            $arr = array('language'=>$lang);
        return $this->createUrl('', $arr);
    }

}