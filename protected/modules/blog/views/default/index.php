<ul>
    <li> <% echo CHtml::link('Все', $this->createUrl('/blog/default/index')) %></li>
    <% foreach(BlogCategory::getCategories()  as $id => $name): %>
        <li>
            <% echo CHtml::link($name, $this->createUrl('/blog/default/index', array('category' => $name)) )%>
        </li>
    <% endforeach;%>
</ul>

<?php $this->widget(
'zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemsCssClass' => 'blog',
    'itemView'=>'_view',
    'ajaxUpdate' => false,
    //'afterAjaxUpdate' => 'function() { window.location.href = "/blog/page/2" }',

    'pager'        => array(
        'class'          => 'CLinkPager',
        'header'=>'',
        'firstPageLabel' => '«',
        'prevPageLabel'  => '«',
        'nextPageLabel'  => '»',
        'lastPageLabel'  => '»',

        'htmlOptions' => array(
            'class'=>'pagination blog-pagination'
        )
    ),
    'template'=>'{items}{pager}'
)
); ?>
