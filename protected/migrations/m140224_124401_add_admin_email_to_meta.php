<?php

class m140224_124401_add_admin_email_to_meta extends CDbMigration
{
    public function safeUp() {
        $this->addColumn('meta', 'admin_email', 'varchar(50) NULL');
    }

    public function safeDown() {
        $this->dropColumn('meta', 'admin_email');
    }
}