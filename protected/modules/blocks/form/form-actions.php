<?php
/**
 * @file form-actions.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 27.02.14 19:25
 */
?>

<div class="form-actions">
    <button class="btn btn-info" type="submit">
        <i class="icon-ok bigger-110"></i>
        <% echo ($model->isNewRecord) ? t('Create') : t('Save'); %>
    </button>
    &nbsp; &nbsp; &nbsp;
    <button class="btn">
        <a href="<% echo $this->createUrl('/' . getCurrentModuleName()) %>">
            <i class="icon-remove bigger-110"></i>
            <% echo t('Cancel')%>
        </a>
    </button>
</div>

