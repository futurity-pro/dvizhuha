<?php
/**
 * @file modules_custom.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 12.02.14 21:07
 */
return array(
    'user',
    'pages'=>array(
        'cacheId'=>'pagesPathsMap',
    ),
    'portfolio',
    'advantage',
    'review',
    'customfields',
    'menumanager',
    'blog',
);