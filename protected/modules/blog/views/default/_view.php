<div class="blog__one-post">
	<h3>
        <% echo CHtml::link($data->title , $this->createUrl('/blog/default/view', array('id' => $data->id, 'alias' => $data->title)) ); %>
    </h3>
    <div class="blog-panel">
        <div class="publication-date pull-left">
            <?php echo CHtml::encode($data->created_at); ?>
        </div>

        <div class="blog-category pull-left">
            <% echo CHtml::link(
                $data->category->name,
                '/' . Yii::app()->request->getPathInfo(). '?category=' . $data->category->name,
                array('class'=>'pull-left')
                ) %>
        </div>
        <div class="clearfix"></div>
    </div>
    <% if(!Yii::app()->user->isGuest) :%>
        <% echo CHtml::link(
            t('Edit'),
            array('/blog/default/update', 'id' => $data->id),
            array('class'=>'pull-left', 'target' => '_blank')
        ) %>
        <div class="clearfix"></div>
    <% endif; %>
    <div class="post-content">
	    <?php echo $data->text; ?>
    </div>
</div>