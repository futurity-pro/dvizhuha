<?php  Yii::app()->clientScript->registerPackage('tiny-mce');?>
<div>
    <div class="widget-box span3">
        <div class="widget-header header-color-green2">
            <h4 class="lighter smaller"><?= t('Pages') ?></h4>
            <?php echo CHtml::link(t('Add Page'), array('/site/pages', 'id'=>-1), array('class'=>'btn btn-primary')) ?>
        </div>

        <div class="widget-body">
            <div class="widget-main padding-8">
                <div id="tree2" class="tree tree-unselectable">
                    <?php foreach($pages as $page): ?>
                    <?php echo CHtml::link('<span class="tree-item-name"><i class="icon-file-text grey"></i> '.$page->title.'</span>', array('/site/pages', 'id'=>$page->id), array('class'=>($model && $page->id==$model->id?'active ':'').'tree-item', 'style'=>'display: block;')) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box span9 pages-content">
        <?php if($model): ?>
        <div class="form">
            <?php echo CHtml::beginForm(); ?>
            <?php echo CHtml::errorSummary($model, null, null, array('class'=>'alert')); ?>

            <div class="row">
                <?php echo CHtml::activeLabel($model,'title'); ?>
                <?php echo CHtml::activeTextField($model,'title'); ?>
            </div>

            <div class="row">
                <?php echo CHtml::activeLabel($model,'url'); ?>
                <?php echo CHtml::activeTextField($model,'url'); ?>
            </div>

            <div class="row">
                <?php echo CHtml::activeLabel($model,'body'); ?>
                <?php echo CHtml::activeTextArea($model,'body', array('rows'=>10)); ?>
            </div>

            <div class="row">
                <?php echo CHtml::activeLabel($model,'description'); ?>
                <?php echo CHtml::activeTextArea($model,'description', array('class'=>'mceNoEditor', 'rows'=>6)); ?>
            </div>

            <div class="row">
                <?php echo CHtml::activeLabel($model,'keywords'); ?>
                <?php echo CHtml::activeTextArea($model,'keywords', array('class'=>'mceNoEditor', 'rows'=>6)); ?>
            </div>

            <br><br>
            <?php echo CHtml::submitButton(t('Save'), array('class'=>'btn btn-primary')); ?>

            <?php echo CHtml::endForm(); ?>
        </div><!-- form -->
        <?php endif; ?>
    </div>
</div>