<?php

class m140303_135523_rename_rubric_field extends CDbMigration
{

    public function up()
    {
        $transaction = $this->getDbConnection()->beginTransaction();
        try {
            $sql = 'ALTER TABLE  `blog_posts` CHANGE  `rubric_id`  `category_id` INT( 10 ) UNSIGNED NOT NULL ';
            $this->execute($sql);
            $transaction->commit();
        }
        catch(Exception $e) {
            echo "Exception: ".$e->getMessage()."\n";
            $transaction->rollBack();
            return false;
        }
    }

    public function down()
    {

    }

}