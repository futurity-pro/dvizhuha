<?php
class DefaultController extends Controller {
    public $layout = 'application.views.layouts.main';


    public function actionView($id)	{

        $page = $this->loadModel($id);
        $this->layout = '//layouts/main';

        $meta = Meta::model()->findByPk( Yii::app()->name  . '(' . Yii::app()->language . ')');

        CoreFunctions::redirectTo(
            $page->redirects,
            $this->createUrl('pages/default/view', array('id' => $page->id))
        );


        $this->render('view', array(
            'page' => $page,
            'meta' => $meta,
        ));
    }

    public function loadModel($id)
    {
        $model = Page::model()->published()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Запрашиваемая страница не существует.');
        }
        return $model;
    }

}