<% $this->beginContent(FuturityModule::pathToBlock('layouts::form')); %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'name',
        ));
    %>


    <% $this->renderPartial( FuturityModule::pathToBlock('form::form-actions'), array('model' => $model)); %>

<% $this->endContent(); %>


