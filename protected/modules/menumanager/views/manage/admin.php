<div class="page-header position-relative">
    <h1>
        <% echo t('Menu'); %>
    </h1>
</div>

<% $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'menu-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'summaryText' => '',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
	'columns'=>array(
		'name',
        array(
            'class'=>'CButtonColumn',
            //'class'=>'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => t('Are you sure you want to delete this item?'),
            'template'=>'{items}{update}{delete}',
            'htmlOptions' => array('class'=>'infopages_buttons_column'),
            'buttons' => array(
                'items' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'url' => 'Yii::app()->createUrl("menumanager/default/admin", array("id"=>$data->id))',
                    'options'=>array('class'=>'icon-folder-close'),

                ),
                'update' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-edit'),

                ),
                'delete' => array(
                    'label'=>'',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-remove' ),

                ),

            ),
        ),
	),
)); %>
