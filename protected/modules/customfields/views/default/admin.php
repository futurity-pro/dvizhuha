<div class="page-header position-relative">
    <h1>
        <% echo tm('Manage Custom Fields'); %>
    </h1>
</div>

<a href="<% echo $this->createUrl('/customfields/default/create') %>" class="btn btn-app btn-primary no-radius">
    <i class="icon-file bigger-230"></i>
    <% echo t('Create') %>
</a>
<span class="label label-large label-yellow arrowed-in">
    <% echo t('you can filter the items by simply typing the beginning of the word and press enter')%>
</span>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#custom-fields-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'custom-field-grid',
	'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
	'filter'=>$model,
    'enableSorting' => true,
    'pagerCssClass'=>'pagination pagination-centered',
    'pager'        => array(
        'class'          => 'CLinkPager',
        'header'=>'',
        //'firstPageLabel' => '«',
        'prevPageLabel'  => t('«'),
        'nextPageLabel'  => t('»'),
        //'lastPageLabel'  => '»',
        'htmlOptions' => array('class'=>'pagination post-pagination')
    ),
	'columns'=>array(
		'title',
		'field_key',
		'value',
        array(
            'class'=>'CButtonColumn',
            //'class'=>'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => t('Are you sure you want to delete this item?'),
            'template'=>'{update}{delete}',
            'htmlOptions' => array('class'=>'infopages_buttons_column'),
            'buttons' => array(
                'update' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-edit'),

                ),
                'delete' => array(
                    'label'=>'',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-remove' ),

                ),
            ),
        ),
	),
)); ?>
