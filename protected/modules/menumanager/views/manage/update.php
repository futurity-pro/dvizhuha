<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Menu Manager'), array('/menumanager/manage/'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo t('Create Item')%>
        </small>
    </h1>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>