<div class="page-header position-relative">
    <h1>
        <% echo tm('Manage Advantage'); %>
    </h1>
</div>

<a href="<% echo $this->createUrl('/advantage/default/create') %>" class="btn btn-app btn-primary no-radius">
    <i class="icon-file bigger-230"></i>
    <% echo t('Create') %>
</a>

<!--<span class="label label-large label-yellow arrowed-in">-->
<!--    --><%// echo t('You can sort element just drag them')%>
<!--</span>-->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    //'orderField' => 'order',
    //'idField' => 'id',
    //'orderUrl' => 'order',
    'summaryText'=>'',
    'itemsCssClass' => 'table table-striped table-bordered table-hover',
    'columns'=>array(
        array(
            'name'=>'visible',
            'filter' => false,
            'sortable' => false,
            'value'=> 'Chtml::openTag("label") .
                            CHtml::checkBox($data->visible, $data->visible, array("class"=>"ajax-change-status" , "data-id"=> $data->id, "id"=> "visible-" . $data->id)).
                            Chtml::openTag("label", array("class"=>"lbl")) . Chtml::closeTag("label").
                       Chtml::closeTag("label")',
            'type'=>'raw',
            'htmlOptions'  => array(),
            'visible'      => true,
        ),
        array(
            'name' => 'title',
            'type' => 'raw',
            'value' => '$data->getTitle()',
            'filter' => false,
            'sortable' => false,
        ),
        array(
            'name' => 'Url',
            'type' => 'raw',
            'value' => '$data->url',
            'filter' => false,
            'sortable' => false,
        ),
        array(
            'class'=>'CButtonColumn',
            //'class'=>'bootstrap.widgets.TbButtonColumn',
            'deleteConfirmation' => tm('Are you sure you want to delete this item?'),
            'template'=>'{update}{delete}',
            'htmlOptions' => array('class'=>'infopages_buttons_column'),
            'buttons' => array(
                'update' => array(
                    'label'=> '',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-edit'),

                ),
                'delete' => array(
                    'label'=>'',
                    'imageUrl'=>'',
                    'options'=>array('class'=>'icon-remove' ),

                ),
            ),
        ),
    ),
)); ?>

<script type="text/javascript">
/* ajax switch checkbox state */
    $('.ajax-change-status').live('click', function(){
        var modelID = $(this).data('id');
        var checkbox = $(this);
        $.ajax({
            'url' : '/advantage/default/ChangeVisibility',
            'data' : { 'id' : modelID },
            success : function(){
                checkbox.parent().trigger('click');
            }
        });
        return false;
    });
</script>