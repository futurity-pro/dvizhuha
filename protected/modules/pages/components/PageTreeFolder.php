<?php

class PageTreeFolder extends CWidget {

	public function run()
	{

        $pages = Page::model()->findAll();
        $level = 0;

        $currentPageID = Yii::app()->getRequest()->getParam('id');
        echo '<div class="widget-box span6">' .
                '<div class="widget-header header-color-green2">' .
                    '<h4 class="lighter smaller">' . t('Site Map') . '</h4>' .
                     CHtml::link('Создать страницу', array('create'), array('class' => 'btn btn-primary')) .
                '</div>' .

                '<div class="widget-body">'.
                    '<div class="widget-main padding-8">'.
                    '<div id="tree1" class="tree tree-selectable">';

        foreach ($pages as $n => $page)	{
            if ($page->level == $level) {
                echo CHtml::closeTag('div') . "\n";
                echo CHtml::closeTag('div') . "\n";

            }
            else if ($page->level > $level) {
                echo CHtml::openTag('div', array('class' => 'tree-folder')) . "\n";
                    echo CHtml::openTag('div', array('class' => 'tree-folder-header'));
                        $nameFolderClass  = ($currentPageID == $page->id) ? 'tree-folder-name current' : 'tree-folder-name';
                        echo CHtml::openTag('div', array('class' => $nameFolderClass));
                            echo ' <i class="icon-file"></i>';
                            echo CHtml::link(  '   '.$page->page_title , array('/pages/admin/update', 'id' => $page->id));
                    echo CHtml::closeTag('div');
                echo CHtml::closeTag('div');

                echo CHtml::openTag('div', array('class' => 'tree-folder-content'));

                $level = $page->level; continue;
            }
            else {

                echo CHtml::closeTag('div') . "\n";

                for ($i = $level - $page->level; $i; $i--)	{
                    echo CHtml::closeTag('div') . "\n";
                    echo CHtml::closeTag('div') . "\n";
                }
            }

            echo CHtml::openTag('div', array('class' => 'tree-folder')) . "\n";
                echo CHtml::openTag('div', array('class' => 'tree-folder-header'));
                $nameFolderClass  = ($currentPageID == $page->id) ? 'tree-folder-name current' : 'tree-folder-name';
                echo CHtml::openTag('div', array('class' => $nameFolderClass));
                        echo ' <i class="icon-file"></i>';
                        echo CHtml::link(  '   '.$page->page_title , array('/pages/admin/update', 'id' => $page->id));
                    echo CHtml::closeTag('div');
                echo CHtml::closeTag('div');


            echo CHtml::openTag('div', array('class' => 'tree-folder-content'));

            $level = $page->level;
        }

        for ($i = $level; $i; $i--)	{
            echo CHtml::closeTag('div') . "\n";
            echo CHtml::closeTag('div') . "\n";
            //echo CHtml::closeTag('div') . "\n";
        }

        echo CHtml::closeTag('div') . "\n";

        echo CHtml::closeTag('div') . "\n";
        echo CHtml::closeTag('div') . "\n";
        echo CHtml::closeTag('div') . "\n";
    }
}