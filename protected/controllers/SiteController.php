<?php
class SiteController extends Controller
{
    public $theme = 'default';
    public $layout = '//layouts/main';
    public function init()
    {
        Yii::app()->setTheme('default');
    }
	public function actionIndex()
	{
        $meta = Meta::model()->findByPk(Yii::app()->name  . '(' . Yii::app()->language . ')');
        if(!empty($meta)){
            $this->metaTitle       = $meta->getMetaTitle();
            $this->metaDescription = $meta->getMetaDescription();
            $this->metaKeywords    = $meta->getMetaKeywords();
        }

        CoreFunctions::redirectTo($meta->redirects, '/');

		$this->render('index');
	}

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error)
        {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionMap()
    {
        $this->layout = '//layouts/empty';
        Yii::app()->setTheme('ace');
        $this->render('map');
    }

    public function actionLogin()
    {
        Yii::app()->setTheme('ace');
        if(Yii::app()->user->isGuest) {
            $this->layout = '//layouts/login';
            $model = new LoginForm;
            if(isset($_POST['ajax']) && $_POST['ajax'] ===' login-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if(isset($_POST['LoginForm']))
            {
                $model->attributes=$_POST['LoginForm'];
                if($model->validate() && $model->login())
                    $this->redirect('/pages');
            }
            $this->render('login',array('model'=>$model));
        } else {
            //@TODO : this link will be based on user role in the future
            $redirectUrl = '/admin';
            $this->redirect($redirectUrl);
        }
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('/');
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
//                $message = new YiiMailMessage;
//                $message->view = 'mail';
//                $message->setSubject('['.t(Yii::app()->name).'] ' . $model->subject);
//                $message->setBody(array('model' => $model), 'text/html');
//                $adminEmail = CoreFunctions::getAdminEmail();
//                $message->addTo($adminEmail);
//                $message->addTo('jehkinen@ya.ru');
//                $message->from = $model->email;

                $adminEmail = CoreFunctions::getAdminEmail();
                if(ajEMailer::send(array(
                    'to'            => array($adminEmail),
                    'subject'       => 'dvizhuha.net',
                    'view'          => 'mail',
                    'data'          => array(
                        'model'     => $model,
                    )
                ))) {

                } else {
                     die();
                }

                Yii::app()->user->setFlash('contact', t('Thank you for contacting us. We will respond to you as soon as possible.'));
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

}