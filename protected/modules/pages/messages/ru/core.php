<?php
/**
 * @file core.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 13.02.14 7:38
 */
return array(
    'Site Map'          => 'Карта Сайта',
    'Type'              => 'Тип страницы',
    'Meta keywords'     => 'Ключевые слова',
    'Meta title'        => 'Мета-заголовок',
    'Page title'        => 'Заголовок',
    'Is published'      => 'Опубликовано',
    'Redirects'         => 'Редиректы',
    'Admin Email'       => 'Email администратора',
);




