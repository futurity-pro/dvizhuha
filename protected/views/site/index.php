
<div class="slides">
    <div class="container">
        <div class="slider-item">
            <img src="<% echo Yii::app()->baseUrl.'/site/img' %>/people.jpg" alt="Slider-1" class="img-responsive">
            <div class="slider-item-caption">
                <h2><% echo CustomField::getCustomFieldByKey('indexPageTitle')%></h2>
                <p><% echo CustomField::getCustomFieldByKey('indexPageTextBelowTitle')%></p>
                <a href="/contact" class="slider-item-btns btns">
                    <% echo CustomField::getCustomFieldByKey('contactWithUs')%>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="homepage">
    <div class="homepage-bg-1">
        <div class="homepage-bg-2">
            <div class="homepage-bg-3">
                <% echo $this->renderPartial('blocks/advantages') %>
                <% echo $this->renderPartial('blocks/works') %>
                <% echo $this->renderPartial('blocks/reviews') %>
            </div>
        </div>
    </div>
</div>
<div class="bottom-bg"></div>