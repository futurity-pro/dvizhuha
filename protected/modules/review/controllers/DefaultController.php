<?php
class DefaultController extends CoreController
{
    public $baseModel = 'Review';
    public $crudFormID = 'review-form';
    public  $imageWidth  = 240;
    public  $imageHeight = 240;
    public $multiLang = true;
}