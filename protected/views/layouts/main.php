<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <title><?php echo CHtml::encode($this->metaTitle); ?></title>
    <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>">
    <meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>">

    <% Yii::app()->clientScript->registerPackage('site'); %>

    <% echo $this->customCodeHead; %>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic&subset=latin,cyrillic-ext,cyrillic,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,800,700,600,300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
</head>

    <body>
        <% echo $this->customCodeAfterOpenBody; %>
        <a style="display: none" href="#" class="call"><span>Задать вопрос консультанту</span>
            <img src="<% echo Yii::app()->baseUrl.'/site/img' %>/background/call.png" alt="" class="img-responsive">
        </a>

        <% $this->renderPartial('//layouts/blocks/header') %>
        <div class="page-content">
            <% echo $content; %>
        </div>
        <% echo $this->renderPartial('//layouts/blocks/footer') %>

        <% echo $this->customCodeBeforeCloseBody; %>
    </body>

</html>
