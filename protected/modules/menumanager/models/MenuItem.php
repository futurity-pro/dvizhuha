<?php
class MenuItem extends CActiveRecord
{
    public $sortableCondition = 'menu_id';

    public function relations()
    {

        return array('menu' => array(self::HAS_ONE, 'Menu', array('menu_id'=>'id')));
    }

	public function tableName()
	{
		return 'menu_item';
	}
	public function rules()
	{
		return array(
			array('url, title', 'required'),
			array('url', 'length', 'max'  => 1000),
			array('title', 'length', 'max' => 100),
			array('menu_id', 'safe'),
			array('id, url, title', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => t('Url'),
			'title' => t('Title'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('menu_id', $this->menu_id, true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function beforeSave(){
        if($this->isNewRecord){
            $menuID = $this->menu_id;
            $maxOrder = Yii::app()->db->createCommand()
                ->select('MAX(`order`) as maxOrder')
                ->where('menu_id = :menu_id', array(':menu_id'=>$menuID))
                ->from($this->tableName())
                ->queryScalar();

            $this->order = $maxOrder + 1;
        }

        return parent::beforeSave();
    }
    public function afterDelete()
    {
        $items = MenuItem::model()->findAll(
            array(
                'order'=> '`order`ASC',
                'condition' => 'menu_id = :mid',
                'params' => array(':mid' => $this->menu_id)
            )
        );
        $order = 1;
        foreach($items as $item) {
            $item->order = $order;
            $item->save();
            $order++;
        }
        return parent::afterDelete();
    }

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
