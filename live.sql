-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: dvizhuha
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advantages`
--

DROP TABLE IF EXISTS `advantages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advantages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `text` text,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `url` varchar(200) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advantages`
--

LOCK TABLES `advantages` WRITE;
/*!40000 ALTER TABLE `advantages` DISABLE KEYS */;
INSERT INTO `advantages` VALUES (3,'2014-02-14 15:23:36','Новые идеи','У нас накоплен большой опыт работы в социальных сетях. Свои собственные наработки в данной области.','1aa9c81abf90725fe4a6ab636657fad0.jpg',1,'/novye-idei','ru'),(6,'2014-02-14 15:23:36','Адекватные цены','Качество для нас – это в первую очередь понимание особенностей Клиента, профиля позиции, представление релевантных кандидатов, своевременная и полноценная обратная связь. Особое внимание мы уделяем взаимодействию с кандидатами на финальных этапах работы по проекту','377447e0a28bb1a85b721e511942297d.jpg',1,'/adekvatnye-ceny','ru'),(7,'2014-02-18 08:51:34','saving Money','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','b763012e16749777980b2631756811cf.jpg',1,'saving-money','en'),(8,'2014-02-18 09:51:14','дуже сильна сторона','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','b29eb7058f616ab1bb235bf81419c522.jpg',1,'','uk'),(9,'2014-02-18 09:51:20','дуже сильна сторона','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','7065f7697b2951c1946a26fe2a82a018.jpg',1,'','uk'),(10,'2014-02-18 09:51:49','дуже сильна сторона','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','fbffac4b099785316299c9dbd9211c1e.jpg',1,'','uk'),(14,'2014-03-18 19:38:37','saving Money 1','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','2ea77f7030fc64b6e75a92aac392088c.jpg',1,'saving-money-1','en'),(15,'2014-03-18 19:43:15','The PAGES','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','3b819a3e4522ec2cd6183f6e6f36d6f2.jpg',1,'the-pages','en'),(16,'2014-03-19 12:49:07','lalal','lalal',NULL,1,'lalal','en'),(17,'2014-04-05 21:50:47','Качество','Качество для нас – это в первую очередь понимание особенностей Клиента, профиля позиции, представление релевантных кандидатов, своевременная и полноценная обратная связь.','24566cd967482c22e206f8a3fea9f2de.jpg',1,'/kachestvo','ru');
/*!40000 ALTER TABLE `advantages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (4,'SMM','ru'),(6,'Spam','en'),(7,'SMO','ru');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `blog_posts_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts`
--

LOCK TABLES `blog_posts` WRITE;
/*!40000 ALTER TABLE `blog_posts` DISABLE KEYS */;
INSERT INTO `blog_posts` VALUES (20,'VK spam','<p>The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.<br></p>','2014-03-17 22:00:00',1,6,1,'en'),(22,'Сколько стоит целевая аудитория?','<blockquote><p><br></p></blockquote><p></p>\r\n','2014-04-26 21:00:00',1,4,1,'ru');
/*!40000 ALTER TABLE `blog_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_key` varchar(100) NOT NULL,
  `value` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
INSERT INTO `custom_fields` VALUES (2,'indexPageTextBelowTitle','студия социального медиа маркетинга (SMM) содержащее в себе комплекс стратегий и мероприятий для про','Текст в баннере сверху','ru'),(3,'contactWithUs','Связаться с нами','текст на кнопке Связаться с нами','ru'),(4,'ownStrongSides','Работая с нами вы получаете','Наши преимущества','ru'),(5,'ownWorksTitle','Наши работы','заголовок `Наши работы`','ru'),(6,'clientsReviewTitle','Отзывы клиентов','заголовок `Отзывы`','ru'),(7,'phoneOnHeader','+38 (095) 742-88-51','phoneOnHeader','ru'),(8,'emailOnHeader','seredun94@gmail.com','Почта вверху','ru'),(9,'bookServiceNowTitle','Закажите услугу прямо сейчас','Текст над розовой кнопкой в футере','ru'),(10,'vkLink','http://vk.com/dvizhuha_net','ccылка на vk','ru'),(11,'phoneOnFooter','+38 (095) 742-88-51','Телефон в футере','ru'),(12,'emailOnFooter','seredun94@gmail.com','Емейл в футере','ru'),(13,'subscribeOnEmailsTitle','Подпишитесь на рассылку','Подписка на рассылку','ru'),(14,'subscribeLinkOnFooter','http://futurity.us3.list-manage.com/subscribe?u=81d6f7a8cf84b15233dfd6f33&id=1fdaccc3ca','Линки подписки на рассылку в футере','ru'),(15,'facebookLink','http://www.facebook.com/dvizhuha','Ссылка на Фейсбук','ru'),(16,'youtubeLink','http://www.youtube.com/channel/UCqhLIDP1doPBRu_b0HoWbBA','Ссылка на Youtube','ru'),(17,'piLink','http://www.pinterest.com/seredun94/','RU Ссылка Пинтерест','ru'),(18,'instagrammLink','http://instagram.com/seredun94','RU Ссылка на Инстаграм','ru'),(19,'InSocialLink','InSocialLink','InSocialLink','ru'),(20,'ssSocialLink','ssSocialLink','ssSocialLink','ru'),(21,'vimeoLink','http://vimeo.com/user26123289','RU Ссылка Вимео','ru'),(22,'twitterSocialLink','http://twitter.com/dvizhuha_net','Ссылка на Твиттер','ru'),(23,'odnoklassnikiSocialLink','http://www.odnoklassniki.ru/group/53065649684580','Ссылка на Одноклассники','ru'),(24,'copyRightOnFooter','© 2013-2014 SMM-агенство \"Движуха\"','Копирайт','ru'),(25,'indexPageTitle','Promoting Your Business on Social Networks','EN Заголовок в хедере','en'),(26,'indexPageTextBelowTitle','Promote your business through social media from the Football Federation of Ukraine received a letter','EN Текст в хедере','en'),(27,'contactWithUs','Contact us','EN Связаться с нами','en'),(28,'ownStrongSides','Working with us you get','EN Наши сильные стороны','en'),(29,'ownWorksTitle','Our works','EN Наши работы','en'),(30,'clientsReviewTitle','Client reviews','EN Отзывы клиентов','en'),(31,'phoneOnHeader','+38 (095) 742-88-51','EN Телефон в хедере','en'),(32,'emailOnHeader','seredun94@gmail.com','EN Email в хедере ','en'),(33,'bookServiceNowTitle','Book a service now','EN Закажите услугу','en'),(34,'vkLink','http://vk.com/dvizhuha_net','EN Ссылка на Вконтакте','en'),(35,'phoneOnFooter','+38 (095) 742-88-51','EN Телефон в футере','en'),(36,'emailOnFooter','seredun94@gmail.com','EN Email в футере','en'),(37,'subscribeOnEmailsTitle','Subscribe to our newsletter','EN Подписка на рассылку','en'),(38,'subscribeLinkOnFooter','Subscribe to our newsletter','EN Подписка на рассылку','en'),(39,'facebookLink','http://www.facebook.com/dvizhuha','EN Ссылка на Фейсбук','en'),(40,'youtubeLink','http://www.youtube.com/channel/UCqhLIDP1doPBRu_b0HoWbBA','EN ссылка на Ютуб','en'),(41,'piLink','piLink','piLink','en'),(42,'instagrammLink','instagrammLink','instagrammLink','en'),(43,'InSocialLink','InSocialLink','InSocialLink','en'),(44,'ssSocialLink','ssSocialLink','ssSocialLink','en'),(45,'vimeoLink','vimeoLink','vimeoLink','en'),(46,'twitterSocialLink','twitterSocialLink','twitterSocialLink','en'),(47,'odnoklassnikiSocialLink','http://www.odnoklassniki.ru/group/53065649684580','EN Ссылка на Одноклассники','en'),(48,'copyRightOnFooter','© 2013-2014 SMM-agency \"Dvizhuha\"','EN Копирайт','en'),(49,'indexPageTitle','Просування вашого бізнесу в соціальних мережах','UA Заголовок в хедере','uk'),(50,'indexPageTextBelowTitle','Просування вашого бізнесу за допомогою соціальних медіа Від Федерації футболу України надійшов лист ','UA Текст в хедере','uk'),(51,'contactWithUs','Зв\'язатися з нами','UA Связаться с нами','uk'),(52,'ownStrongSides','Працюючи з нами ви отримуєте','UA Наши сильные стороны','uk'),(53,'ownWorksTitle','Наші роботи','UA Наши работы','uk'),(54,'clientsReviewTitle','Відгуки клієнтів','UA Отзывы клиентов','uk'),(55,'phoneOnHeader','+38 (095) 742-88-51','UA Телефон в хедере','uk'),(56,'emailOnHeader','seredun94@gmail.com','UA Email в хедере','uk'),(57,'bookServiceNowTitle','Замовте послугу прямо зараз','UA Закажите услугу прямо сейчас','uk'),(58,'vkLink','http://vk.com/dvizhuha_net','UA Ссылка на ВК','uk'),(59,'phoneOnFooter','+38 (095) 742-88-51','UA Телефон в футере','uk'),(60,'emailOnFooter','seredun94@gmail.com','UA Email в футере','uk'),(61,'subscribeOnEmailsTitle','Підпишіться на розсилку','UA Подписка на рассылку','uk'),(62,'subscribeLinkOnFooter','http://futurity.us3.list-manage.com/subscribe?u=81d6f7a8cf84b15233dfd6f33&id=1fdaccc3ca','subscribeLinkOnFooter','uk'),(63,'facebookLink','http://www.facebook.com/dvizhuha','UA Ссылка на Фейсбук','uk'),(64,'youtubeLink','http://www.youtube.com/channel/UCqhLIDP1doPBRu_b0HoWbBA','UA ссылка на Ютуб','uk'),(65,'piLink','piLink','piLink','uk'),(66,'instagrammLink','instagrammLink','instagrammLink','uk'),(67,'InSocialLink','InSocialLink','InSocialLink','uk'),(68,'ssSocialLink','ssSocialLink','ssSocialLink','uk'),(69,'vimeoLink','vimeoLink','vimeoLink','uk'),(70,'twitterSocialLink','twitterSocialLink','twitterSocialLink','uk'),(71,'odnoklassnikiSocialLink','http://www.odnoklassniki.ru/group/53065649684580','UA ссылка на Одноклассники','uk'),(72,'copyRightOnFooter','© 2013-2014 SMM-агенство \"Движуха\"','UA Копирайт','uk'),(73,'contactWithUsLink','contactWithUsLink','contactWithUsLink','ru'),(74,'contactWithUsTitle','Свяжитесь с нами','заголовок \'Свяжитесь с нами\'','ru'),(75,'contactWithUsText','Мы свяжемся с вами в течении суток','Текст на странице обратной формы','ru'),(76,'fieldRequiredForFillingText','Поля, отмеченные звёздочками обязательны для заполнения','fieldRequiredForFillingText','ru'),(77,'contactWithUsTitle','Зв\'яжіться з нами ','UA Заголовок Обратная форма','uk'),(78,'contactWithUsText','Ми зв\'яжемося з вами на протязі доби','UA Текст Обратная форма','uk'),(79,'fieldRequiredForFillingText','обов\'язкові поля','UA Обязательные поля','uk'),(80,'phoneOnHeader','phoneOnHeader','phoneOnHeader',''),(81,'emailOnHeader','emailOnHeader','emailOnHeader',''),(82,'bookServiceNowTitle','bookServiceNowTitle','bookServiceNowTitle',''),(83,'contactWithUs','contactWithUs','contactWithUs',''),(84,'phoneOnFooter','phoneOnFooter','phoneOnFooter',''),(85,'emailOnFooter','emailOnFooter','emailOnFooter',''),(86,'subscribeOnEmailsTitle','subscribeOnEmailsTitle','subscribeOnEmailsTitle',''),(87,'subscribeLinkOnFooter','subscribeLinkOnFooter','subscribeLinkOnFooter',''),(88,'vkLink','vkLink','vkLink',''),(89,'facebookLink','facebookLink','facebookLink',''),(90,'youtubeLink','youtubeLink','youtubeLink',''),(91,'piLink','piLink','piLink',''),(92,'instagrammLink','instagrammLink','instagrammLink',''),(93,'InSocialLink','InSocialLink','InSocialLink',''),(94,'ssSocialLink','ssSocialLink','ssSocialLink',''),(95,'vimeoLink','vimeoLink','vimeoLink',''),(96,'twitterSocialLink','twitterSocialLink','twitterSocialLink',''),(97,'odnoklassnikiSocialLink','odnoklassnikiSocialLink','odnoklassnikiSocialLink',''),(98,'copyRightOnFooter','copyRightOnFooter','copyRightOnFooter',''),(99,'indexPageTitle','indexPageTitle','indexPageTitle',''),(100,'indexPageTextBelowTitle','indexPageTextBelowTitle','indexPageTextBelowTitle',''),(101,'ownStrongSides','ownStrongSides','ownStrongSides',''),(102,'ownWorksTitle','ownWorksTitle','ownWorksTitle',''),(103,'clientsReviewTitle','clientsReviewTitle','clientsReviewTitle',''),(104,'contactWithUsTitle','Contact Us','EN Заголовок обратной связи','en'),(105,'contactWithUsText','We will contact you during the day','EN Текст обратной связи','en'),(106,'fieldRequiredForFillingText','mandatory fields','EN Обязательные поля','en'),(107,'indexPageTitle','Продвижение вашего бизнеса в социальных сетях','Заголовок в баннере сверху','ru'),(108,'googlePlusSocialLink','https://plus.google.com/b/112757744754935687616/112757744754935687616','Ссылка на Google +','ru'),(109,'googlePlusSocialLink','http://plus.google.com/b/112757744754935687616/112757744754935687616','EN Ссылка на Google +','en'),(110,'googlePlusSocialLink','http://plus.google.com/b/112757744754935687616/112757744754935687616','UA Ссылка на Google + ','uk'),(111,'madeInFuturity','Сделано в Futurity','madeInFuturity','ru'),(112,'madeInFuturity','made In Futurity','madeInFuturity','en'),(113,'madeInFuturity','Зроблено в Futurity','madeInFuturity','uk'),(114,'contactFormName','contactFormName','contactFormName','uk'),(115,'contactFormEmail','contactFormEmail','contactFormEmail','uk'),(116,'contactFormTitle','contactFormTitle','contactFormTitle','uk'),(117,'contactFormPhone','contactFormPhone','contactFormPhone','uk'),(118,'contactFormMessage','contactFormMessage','contactFormMessage','uk'),(119,'contactFormName','* Имя','Поле \"Имя\" в форме контактов','ru'),(120,'contactFormEmail','* E-mail','contactFormEmail','ru'),(121,'contactFormTitle','* Тема','contactFormTitle','ru'),(122,'contactFormPhone','* Телефон','contactFormPhone','ru'),(123,'contactFormMessage','* Сообщение','contactFormMessage','ru'),(124,'contactFormName','* Name','contactFormName','en'),(125,'contactFormEmail','* E-mail','contactFormEmail','en'),(126,'contactFormTitle','* Subject','contactFormTitle','en'),(127,'contactFormPhone','* Phone','contactFormPhone','en'),(128,'contactFormMessage','* Message','contactFormMessage','en');
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `menu_key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'меню в хеадере','ru','header'),(2,'меню в футере','ru','footer'),(3,'EN меню в хедере','en','header'),(7,'EN меню в футере','en','footer'),(8,'UA Меню в хедере','uk','header'),(9,'UA Меню в футере','uk','footer');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(1000) NOT NULL,
  `title` varchar(100) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `visible` int(1) unsigned NOT NULL DEFAULT '1',
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `menu_item_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_item`
--

LOCK TABLES `menu_item` WRITE;
/*!40000 ALTER TABLE `menu_item` DISABLE KEYS */;
INSERT INTO `menu_item` VALUES (2,'/kejsy','Кейсы',2,1,1),(4,'/o-nas','О нас',1,1,1),(5,'/uslugi','Услуги',4,1,1),(6,'/blog','Блог',5,1,1),(20,'/kontakty','Контакты',6,1,1),(32,'/tarify','Тарифы',3,1,1),(33,'/blog','Блог',5,1,2),(34,'/uslugi','Услуги',3,1,2),(47,'/o-nas','О нас',1,1,2),(48,'/kejsy','Кейсы',2,1,2),(49,'/kontakty','Контакты',6,1,2),(50,'/tarify','Тарифы',4,1,2),(51,'/poslugi','Послуги',4,1,8),(53,'/blog','Блог',5,1,8),(55,'/pro-nas','Про нас',1,1,8),(57,'/taryfy','Тарифи',3,1,8),(58,'/kejsy','Кейси',2,1,8),(59,'/kontaktu','Контакти',6,1,8),(60,'/about-us','About us',1,1,3),(61,'/portfolio','Portfolio',2,1,3),(62,'/tariffs','Tariffs',3,1,3),(63,'/services','Services',4,1,3),(64,'/blog','Blog',5,1,3),(66,'/contacts','Contacts',6,1,3),(67,'/about-us','About us',1,1,7),(68,'/portfolio','Portfolio',2,1,7),(69,'/tariffs','Tariffs',3,1,7),(70,'/services','Services',4,1,7),(71,'/blog','Blog',5,1,7),(72,'/contacts','Contacts',6,1,7),(73,'/ukr','Украинский',1,1,9);
/*!40000 ALTER TABLE `menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta` (
  `id` varchar(50) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `cm_head` text,
  `cm_after_open_body` text,
  `cm_before_close_body` text,
  `cm_footer` text,
  `meta_template_title` text,
  `meta_template_description` text,
  `meta_template_keywords` text,
  `redirects` text,
  `lang` varchar(2) NOT NULL,
  `admin_email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta`
--

LOCK TABLES `meta` WRITE;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
INSERT INTO `meta` VALUES ('Движуха','Главная','1234','1234','<!-- Start SiteHeart code -->\r\n<script>\r\n(function(){\r\nvar widget_id = 680568;\r\n_shcp =[{widget_id : widget_id}];\r\nvar lang =(navigator.language || navigator.systemLanguage \r\n|| navigator.userLanguage ||\"en\")\r\n.substr(0,2).toLowerCase();\r\nvar url =\"widget.siteheart.com/widget/sh/\"+ widget_id +\"/\"+ lang +\"/widget.js\";\r\nvar hcc = document.createElement(\"script\");\r\nhcc.type =\"text/javascript\";\r\nhcc.async =true;\r\nhcc.src =(\"https:\"== document.location.protocol ?\"https\":\"http\")\r\n+\"://\"+ url;\r\nvar s = document.getElementsByTagName(\"script\")[0];\r\ns.parentNode.insertBefore(hcc, s.nextSibling);\r\n})();\r\n</script>\r\n<!-- End SiteHeart code -->','<script>\r\nconsole.log(\'after open body\');\r\n</script>\r\n','<script>\r\nconsole.log(\'before close body\');\r\n</script>\r\n','<script>\r\nconsole.log(\'footer\');\r\n</script>','Движуха - {page-title} ','Движуха портал о всякой движухе {page-description} ','двигать, солнце, ещё одно ключевое слово {page-keywords} ',NULL,'',NULL),('Движуха(en)','Dvizhuha','SMM-agensy','','','','','<!-- Start SiteHeart code -->\r\n<script>\r\n(function(){\r\nvar widget_id = 680568;\r\n_shcp =[{widget_id : widget_id}];\r\nvar lang =(\"en\")\r\n.substr(0,2).toLowerCase();\r\nvar url =\"widget.siteheart.com/widget/sh/\"+ widget_id +\"/\"+ lang +\"/widget.js\";\r\nvar hcc = document.createElement(\"script\");\r\nhcc.type =\"text/javascript\";\r\nhcc.async =true;\r\nhcc.src =(\"https:\"== document.location.protocol ?\"https\":\"http\")\r\n+\"://\"+ url;\r\nvar s = document.getElementsByTagName(\"script\")[0];\r\ns.parentNode.insertBefore(hcc, s.nextSibling);\r\n})();\r\n</script>\r\n<!-- End SiteHeart code -->','','','','','en',''),('Движуха(InSocialLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'In',NULL),('Движуха(instagrammLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'in',NULL),('Движуха(piLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'pi',NULL),('Движуха(ru)','Главная','Продвижение в социальных медиа','движуха, кипарис, {page-keywords}','<meta name=\"google-site-verification\" content=\"k0l2IAKoPXZVixT7mW-NAhZpU4SnQNlSAGjpA7cK0W8\" />\r\n\r\n<meta name=\'yandex-verification\' content=\'7fc3f0486df687e5\' />\r\n\r\n<meta name=\"p:domain_verify\" content=\"0547b273a04ac50dd1119f2541bc780e\"/>\r\n','','','<!-- Start SiteHeart code -->\r\n<script>\r\n(function(){\r\nvar widget_id = 680568;\r\n_shcp =[{widget_id : widget_id}];\r\nvar lang =(navigator.language || navigator.systemLanguage \r\n|| navigator.userLanguage ||\"en\")\r\n.substr(0,2).toLowerCase();\r\nvar url =\"widget.siteheart.com/widget/sh/\"+ widget_id +\"/\"+ lang +\"/widget.js\";\r\nvar hcc = document.createElement(\"script\");\r\nhcc.type =\"text/javascript\";\r\nhcc.async =true;\r\nhcc.src =(\"https:\"== document.location.protocol ?\"https\":\"http\")\r\n+\"://\"+ url;\r\nvar s = document.getElementsByTagName(\"script\")[0];\r\ns.parentNode.insertBefore(hcc, s.nextSibling);\r\n})();\r\n</script>\r\n<!-- End SiteHeart code -->','Движуха','Движуха - описание {page-description}','','','ru','madr0001@yandex.ru'),('Движуха(ssSocialLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ss',NULL),('Движуха(twitterSocialLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'tw',NULL),('Движуха(uk)','Главная','Продвижение в социальных медиа','','<meta name=\"google-site-verification\" content=\"k0l2IAKoPXZVixT7mW-NAhZpU4SnQNlSAGjpA7cK0W8\" />\r\n\r\n<meta name=\'yandex-verification\' content=\'7fc3f0486df687e5\' />\r\n\r\n<meta name=\"p:domain_verify\" content=\"0547b273a04ac50dd1119f2541bc780e\"/>','','','<!-- Start SiteHeart code -->\r\n<script>\r\n(function(){\r\nvar widget_id = 680568;\r\n_shcp =[{widget_id : widget_id}];\r\nvar lang =(navigator.language || navigator.systemLanguage \r\n|| navigator.userLanguage ||\"en\")\r\n.substr(0,2).toLowerCase();\r\nvar url =\"widget.siteheart.com/widget/sh/\"+ widget_id +\"/\"+ lang +\"/widget.js\";\r\nvar hcc = document.createElement(\"script\");\r\nhcc.type =\"text/javascript\";\r\nhcc.async =true;\r\nhcc.src =(\"https:\"== document.location.protocol ?\"https\":\"http\")\r\n+\"://\"+ url;\r\nvar s = document.getElementsByTagName(\"script\")[0];\r\ns.parentNode.insertBefore(hcc, s.nextSibling);\r\n})();\r\n</script>\r\n<!-- End SiteHeart code -->','Движуха','Движуха - описание {page-description}','','','uk','madr0001@yandex.ru'),('Движуха(vimeoLink)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'vi',NULL);
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cm_before_content` text,
  `cm_after_content` text,
  `root` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(10) unsigned NOT NULL,
  `rgt` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(127) NOT NULL,
  `layout` varchar(15) DEFAULT NULL,
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `page_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 -page, 1 - folder',
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  `redirects` text,
  `module` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `root` (`root`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (32,'','',32,1,28,1,0,'uslugi',NULL,1,'Услуги','<h2 style=\"text-align: center;\">Наши услуги</h2>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<p style=\"text-align: center;\"></p>\r\n\r\n<table id=\"table53350\"><thead></thead>\r\n<tbody>\r\n	<tr>\r\n		<td class=\"\" style=\"text-align: center;\"><b>Создание и оформление&nbsp;</b></td>\r\n\r\n		<td class=\"redactor-current-td\" style=\"text-align: center;\"><b class=\"redactor-current-td\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Продвижение &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b></td><td class=\"\" style=\"text-align: center;\"><b>&nbsp;Комплексная поддержка</b></td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-soobshestv-vkontakte\" target=\"_blank\" class=\"redactor-current-td\">Вконтакте</a><br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Вконтакте</td><td class=\"\" style=\"text-align: center;\">&nbsp;Вконтакте</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-soobshestv-v-facebook\" target=\"_blank\" class=\"redactor-current-td\">Facebook</a>&nbsp;<br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Facebook</td><td class=\"\" style=\"text-align: center;\">&nbsp;Facebook</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-soobshestv-v-odnoklassnikah\" target=\"_blank\" class=\"redactor-current-td\">Одноклассники</a><br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;&nbsp;Одноклассники</td><td class=\"\" style=\"text-align: center;\">&nbsp;&nbsp;Одноклассники</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-akkaunta-v-instagram\" target=\"_blank\" class=\"redactor-current-td\">Instagram</a>&nbsp;<br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Instagram</td><td class=\"\" style=\"text-align: center;\">&nbsp;Instagram</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-kanala-v-youtube\" target=\"_blank\" class=\"redactor-current-td\">YouTube</a>&nbsp;<br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;&nbsp;YouTube</td><td class=\"\" style=\"text-align: center;\">&nbsp;&nbsp;YouTube</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\"><a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-akkaunta-v-twitter\" target=\"_blank\">Twitter</a>&nbsp;<br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Twitter</td><td class=\"\" style=\"text-align: center;\">&nbsp;Twitter</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;<a href=\"http://dvizhuha.net/uslugi/sozdanie-i-oformlenie-soobshestv-v-google-plus\" target=\"_blank\">Google+</a><br></td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Google+</td><td class=\"\" style=\"text-align: center;\">&nbsp;Google+</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Мой Мир</td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Мой Мир</td><td class=\"\" style=\"text-align: center;\">&nbsp;Мой Мир</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;LinkidIn</td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;LinkidIn</td><td class=\"\" style=\"text-align: center;\">LinkidIn&nbsp;</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Блоги</td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Блоги</td><td class=\"\" style=\"text-align: center;\">&nbsp;Блоги</td></tr><tr>\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Форумы</td>\r\n\r\n		<td class=\"\" style=\"text-align: center;\">&nbsp;Форумы</td><td class=\"\" style=\"text-align: center;\">&nbsp;Форумы</td></tr>\r\n</tbody>\r\n</table>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n\r\n<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"></blockquote>\r\n','Услуги','','',0,'ru',NULL,''),(33,'<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <title>Примеры. Знакомство с JavaScript API. Простой вызов карты.</title>\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <script src=\"http://api-maps.yandex.ru/1.1/index.xml?key=ALw0XFMBAAAA_CcuVQIAqRJido2g6EhCG6QwnwDHnDiFoAwAAAAAAAAAAAAKxzAcPd7DRGHqW_mLQZ4LkCpa8Q==\"\r\n	type=\"text/javascript\"></script>\r\n    <script type=\"text/javascript\">\r\n        window.onload = function () {\r\n            var map = new YMaps.Map(document.getElementById(\"YMapsID\"));\r\n            map.setCenter(new YMaps.GeoPoint(37.64, 55.76), 10);\r\n        }\r\n    </script>\r\n</head>\r\n<body>\r\n    <div id=\"YMapsID\" style=\"width:650px;height:400px\"></div>\r\n</body>\r\n</html>','',33,1,2,1,0,'kontakty',NULL,1,'Контакты','<p><br></p>','Контакты','','',0,'ru',NULL,''),(34,'','<iframe src=\"https://docs.google.com/forms/d/1-r5NhH-_Lvng0mMMckoH49mxFmvuo3hE3daAm7CHNB4/viewform?embedded=true\" width=\"760\" height=\"500\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Загрузка...</iframe>',32,4,5,2,32,'prodvizhenie-youtube',NULL,1,'Продвижение YouTube','<p></p>\r\n','Продвижение Youtube','','',0,'ru',NULL,''),(35,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?105\"></script>\r\n<script type=\"text/javascript\">\r\n  VK.init({apiId: 4222086, onlyWidgets: true});\r\n\r\n(function(d, s, id) {\r\n  var js, fjs = d.getElementsByTagName(s)[0];\r\n  if (d.getElementById(id)) return;\r\n  js = d.createElement(s); js.id = id;\r\n  js.src = \"//connect.facebook.net/ru_RU/all.js#xfbml=1\";\r\n  fjs.parentNode.insertBefore(js, fjs);\r\n}(document, \'script\', \'facebook-jssdk\'));\r\n</script>\r\n\r\n','',35,1,2,1,0,'blog',NULL,1,'Блог','','Блог о движухе','','',0,'ru',NULL,'blog'),(36,'','',36,1,2,1,0,'kejsy',NULL,1,'Кейсы','<p>В разработке</p>\r\n','kejsy','','',0,'ru',NULL,''),(39,'','',39,1,2,1,0,'blog',NULL,1,'Blog','','Blog','','',0,'en',NULL,'Blog'),(40,'','',40,1,4,1,0,'services',NULL,1,'Services','<p><a href=\"http://dvizhuha.net/services/promotion-in-youtube\" target=\"\">Promotion in Youtube</a>&nbsp;<br></p>','SERVICES','','',0,'en',NULL,''),(41,'','',40,2,3,2,40,'promotion-in-youtube',NULL,1,'Promotion in Youtube','<p>The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.<br></p>\r\n','Promotion in Youtube','','',0,'en',NULL,''),(42,'','',42,1,2,1,0,'polsugi',NULL,1,'Полсуги','','Полсуги','','',0,'uk',NULL,''),(43,'','',43,1,2,1,0,'chyasya',NULL,1,'чяся','','чяся','','',0,'uk',NULL,''),(45,'','',45,1,2,1,0,'tarify',NULL,1,'Тарифы','<table id=\"table33988\" class=\"redactor-current-td\">\r\n<tbody>\r\n	\r\n\r\n	<tr>\r\n		<td class=\"\">&nbsp;<b>Начальный&nbsp;</b></td>\r\n\r\n		<td class=\"\">&nbsp;<b>Стабильный&nbsp;</b></td>\r\n\r\n		<td class=\"\">&nbsp;<b>Максимальный</b></td>\r\n	</tr><tr>\r\n		<td class=\"redactor-current-td\">Разработка<br>\r\nи реализация<br>\r\nSMM-стратегии<br>\r\n</td>\r\n\r\n		<td class=\"\">Разработка<br>и реализация<br>SMM-стратегии<br>\r\n</td>\r\n\r\n		<td class=\"\">Разработка<br>и реализация<br>SMM-стратегии<br>\r\n</td>\r\n	</tr><tr>\r\n		<td class=\"\">&nbsp;Разработка дизайнов</td>\r\n\r\n		<td class=\"\">&nbsp;Разработка дизайнов</td>\r\n\r\n		<td class=\"\">&nbsp;Разработка дизайнов</td>\r\n	</tr><tr>\r\n		<td class=\"\">&nbsp;</td>\r\n\r\n		<td class=\"\">&nbsp;</td>\r\n\r\n		<td class=\"\">&nbsp;</td>\r\n	</tr><tr>\r\n		<td class=\"\">&nbsp;</td>\r\n\r\n		<td class=\"\">&nbsp;</td>\r\n\r\n		<td class=\"\">&nbsp;</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n','Тарифы','','',0,'ru',NULL,''),(46,'','<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 0, width: \"650\", height: \"250\", color1: \'FFFFFF\', color2: \'2B587A\', color3: \'5B7FA6\'}, 57412552);\r\n</script>',46,1,2,1,0,'o-nas',NULL,1,'О нас','<p>Мы студия социального медиа маркетинга (SMM) содержащее в себе комплекс стратегий и мероприятий для продвижения и распространения информации через социальные сети.</p><h2 style=\"text-align: center;\"><a href=\"http://seredun94.ecommtools.com/affsignup/\" target=\"\">Стать партнером</a>&nbsp;<br></h2><p><br></p>\r\n','О НАС','','',0,'ru',NULL,''),(48,'','',32,6,7,2,32,'prodvizhenie-v-facebook',NULL,1,' Продвижение в Facebook','<h2>ПРОДВИЖЕНИЕ В FACEBOOK</h2>\r\n\r\n<p>«Facebook» (Фейсбук) носит характер динамично развивающейся социальной сети. По количеству пользователей он занимает второе место в России, отдав первенство сети «ВКонтакте». Количество его участников уже перевалило за отметку 900 миллионов, объединив в себе интеллектуальных пользователей с активной жизненной и социальной позицией, совершающих большое число покупок через всемирную сеть. Число посетителей сайта составляет около 500 миллионов человек в день.&nbsp;<br>\r\n<br>\r\n<b>Дата создания:</b>&nbsp;4 февраля 2004 г.<br>\r\n<b>Пользователи:</b>&nbsp;более 901 000 000<em>Аудитория сайта (по РФ):</em></p>\r\n\r\n<p>от 14 до 20 лет - 17%<br>\r\n от 21 до 30 лет - 27%<br>\r\n от 31 до 40 лет - 32%<br>\r\n от 41 до 55 лет - 24%</p>\r\n<hr><h3>ПРОДВИЖЕНИЕ ПРОФИЛЯ НА FACEBOOK</h3>\r\n\r\n<p>каукук</p>\r\n\r\n<p>Смысл продвижения заключается в обмене информацией о какой-либо компании, услуге или же продукции среди своих друзей и знакомых, которые аналогичным способом делятся этой же информацией, но уже в кругу своего общения. Для этого вида рекламы достаточно иметь свой аккаунт, с помощью которого можно найти заинтересованную аудиторию.</p>\r\n\r\n<p><br>\r\n<br>\r\n</p>\r\n<h3>ПРОДВИЖЕНИЕ ГРУППЫ В FACEBOOK</h3>\r\n\r\n<p>Создание групп в «Facebook» (Фейсбук) связано со стремлением объединения участников социальной сети, интересы которых пересекаются в пределах одной общей темы, например одежда известного бренда. Являясь представителем этой фирмы, вы напрямую заинтересованы в продвижении данной группы, которая станет для вас отличной рекламой.</p>\r\n\r\n<p><br>\r\n<br>\r\n</p>\r\n<h2>ПРОДВИЖЕНИЕ СТРАНИЦЫ В FACEBOOK</h2>\r\n\r\n<p>Социальные сети – хорошая платформа для рекламы в любом направлении бизнеса, чем и является специально созданная страница в «Facebook» (Фейсбук). Ее эффективность объясняется доступностью для любого пользователя, а кнопка «like» (мне нравится) автоматически позволит получать абсолютно все новости со страницы, кроме того распространит их среди всех друзей, привлекая таким образом многочисленных клиентов.</p>\r\n\r\n<p><br>\r\n<br>\r\n</p>\r\n<h2>РЕКЛАМА В ПРИЛОЖЕНИЯХ</h2>\r\n\r\n<p>Внедрение рекламы в приложения социальных сетей положительно сказывается на развитии бизнеса, при этом имеет смысл как создание новых приложений с последующей их раскруткой, так и финансирование существующих. Такой вид рекламы наиболее успешный, несмотря на существенные материальные затраты.</p>\r\n\r\n<p><br>\r\n<br>\r\n</p>\r\n<h2>ТАРГЕТИРОВАНАЯ РЕКЛАМА В FACEBOOK</h2>\r\n\r\n<p>Такой вид рекламы является наиболее нацеленным, способным принести результат максимально быстро. С помощью специальных настроек информация будет направляться только тем пользователям, которых она может заинтересовать. Например, людям, состоящим в браке и имеющих детей 3-5-летнего возраста.</p>',' Продвижение в Facebook','','',0,'ru',NULL,''),(49,'','',32,8,9,2,32,'prodvizhenie-v-odnoklassnikah',NULL,1,'Продвижение в Одноклассниках ','','Продвижение в Одноклассниках ','','',0,'ru',NULL,''),(50,'','',32,10,11,2,32,'prodvizhenie-v-linkedin',NULL,1,'Продвижение в LinkedIn','','Продвижение в LinkedIn','','',0,'ru',NULL,''),(51,'','',32,2,3,2,32,'prodvizhenie-v-blogah',NULL,1,'Продвижение в Блогах ','','Продвижение в Блогах ','','',0,'ru',NULL,''),(52,'','<!-- Начало кода EcommTools.com -->\r\n<a href=\"http://seredun94.ecommtools.com/buy/1/?refid=\" target=\"_blank\">\r\n<img src=\"http://static.ecommtools.com/images/checkout3_ru.gif\" border=\"0\" alt=\"Купить\" title=\"Купить\" style=\"border-style:none\" onClick=\"window.open(\'http://seredun94.ecommtools.com/buy/1/?refid=&pselect=&channel=\',\'\',\'top=200,left=350,width=350,height=250,scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=0,statusbar=0\');return false\" />\r\n</a>\r\n<!-- Конец кода EcommTools.com -->\r\n\r\n<!--\r\n********************************************************************************************\r\nВНИМАНИЕ! Если вы работаете в визуальном редакторе (когда отображается красивое оформление своей страницы),\r\n\r\n1) ОБЯЗАТЕЛЬНО ПЕРЕКЛЮЧИТЕ РЕДАКТОР НА РЕЖИМ \"ИСХОДНЫЙ КОД\" (аналогичные названия: \"Текст\", \"HTML Source\", \"разметка HTML\").\r\n\r\n2) ОБЯЗАТЕЛЬНО ПЕРЕКЛЮЧИТЕ РЕДАКТОР НА РЕЖИМ \"ИСХОДНЫЙ КОД\" (аналогичные названия: \"Текст\", \"HTML Source\", \"разметка HTML\").\r\n\r\n3) ОБЯЗАТЕЛЬНО ПЕРЕКЛЮЧИТЕ РЕДАКТОР НА РЕЖИМ \"ИСХОДНЫЙ КОД\" (аналогичные названия: \"Текст\", \"HTML Source\", \"разметка HTML\").\r\n\r\nВы должны будете увидеть исходную текстовую версию вашей страницы со специальными HTML тегами, и только потом вставляйте полученный код,\r\nв противном случае кнопка покупки не будет работать! Удалите этот текст из полученного кода после прочтения.\r\n\r\nАдминистрация EcommTools приносит извинения за подобное сообщение, это вынужденная и крайняя мера,\r\nт.к. случаи \"полной информационной слепоты\" происходят все чаще и чаще.\r\n\r\nЕсли кто-то из горе-продавцов напишет очередное письмо, что его кнопка \"Купить\" не работает, и после расследования выяснится,\r\nчто визуальный редактор исказил код, на аккаунт продавца будет наложен штраф в 1500 рублей за полное пренебрежение и игнорирование важной информации.\r\nКаждое подобное расследование \"ошибки\" требует переписки, перехода на сайт продавца и изучения исходного кода страницы,\r\nна что тратится время, а время это самый ценный ресурс.\r\n********************************************************************************************\r\n-->\r\n',32,12,13,2,32,'sozdanie-i-oformlenie-grupp',NULL,1,'Создание и оформление групп','','Создание и оформление групп','','',0,'ru',NULL,''),(53,'','',32,14,15,2,32,'sozdanie-i-oformlenie-soobshestv-vkontakte',NULL,1,'Создание и оформление сообществ Вконтакте','<h2>Создание и оформление сообществ Вконтакте</h2>\r\n\r\n<p>Среди пользователей Интернета в России социальная сеть «ВКонтакте» получила наибольшую популярность, обладая при этом огромной базой данных клиентов, что необходимо для любого бизнеса. Современная аудитория сайта, насчитывающая более 120 000 000 человек, способна быстро реагировать на внедрение новых сервисов, а также осуществлять покупки через Интернет.&nbsp;<br>\r\n<br>\r\n<b>Дата создания:</b>&nbsp;2006 г.<br>\r\n<b>Пользователи:</b>&nbsp;более 120 000 000&nbsp;<em>Аудитория сайта:</em></p>\r\n\r\n<p>от 14 до 20 лет - 38%<br>\r\nот 21 до 30 лет - 28%<br>\r\nот 31 до 40 лет - 19%<br>\r\nот 41 до 55 лет - 15%</p>\r\n<h2>ПРОДВИЖЕНИЕ ПРОФИЛЯ ВКОНТАКТЕ</h2>\r\n<br>\r\n\r\n<p>PR-кампания «ВКонтакте» позволяет эффективно продвигать аккаунт конкретного человека. Продвижение персональной страницы происходит путем добавления пользователей в список друзей, информированием их в новостях, с вступлениями в группу и написанием комментариев.</p>\r\n<br>\r\n<br>\r\n<h2>ПРОДВИЖЕНИЕ ГРУППЫ ВКОНТАКТЕ</h2>\r\n<br>\r\n\r\n<p>Специально созданные группы «ВКонтакте» способствуют развитию престижа организации, ее продвижению в конкурентно способной среде и популяризации бренда, оповещая пользователей о новых рекламных акциях и услугах. Привлечение внимания аудитории осуществляется при помощи рассылки приглашений на вступление в соответствующую группу или же контекстной рекламы внутри социальной сети.</p>\r\n<br>\r\n<br>\r\n<h2>ПРИГЛАШЕНИЯ НА ВСТРЕЧИ</h2>\r\n<br>\r\n\r\n<p>Приглашение на встречу – качественный метод оповещения широкого круга пользователей о каком-либо событии в ближайшем времени (например, музыкальный фестиваль, театральное представление, спортивное мероприятие и т.д.). Исходя из интересов, выбираются пользователи, которые посредством рассылки получают приглашения на страницу встречи, где расположена подробная информация о будущем мероприятии.</p>\r\n<br>\r\n<br>\r\n<h2>РЕКЛАМА В ПРИЛОЖЕНИЯХ</h2>\r\n<br>\r\n\r\n<p>Активность пользователей сети в приложениях создает благоприятные условия для распространения в них баннерной рекламы, что позволяет повысить рейтинг товара или всего бренда и заслужить расположение потенциальных клиентов.</p>\r\n<br>\r\n<br>\r\n<h2>ТАРГЕТИРОВАННАЯ РЕКЛАМА ВКОНТАКТЕ</h2>\r\n<br>\r\n\r\n<p>Получение быстрого результата от продвижения на сайте «ВКонтакте» возможно прибегая к помощи контекстной рекламы. Достижение цели осуществляется путем размещения контекстных объявлений, “продакт плейсментов” или баннеров, появляющихся на страницах пользователей.</p>\r\n<br>\r\n<br>\r\n<h2>РАЗМЕЩЕНИЕ ФОТО И ВИДЕО</h2>\r\n<br>\r\n\r\n<p>Рекламные фотоматериалы и видеоролики о новых акциях, товарах и услугах, размещенные в тематических группах, также являются эффективным приемом PR-кампании. Комментарии и отзывы, оставленные пользователями, налаживают взаимосвязь с брендом, продвигая его.</p>\r\n<h2 style=\"text-align: center;\"><span style=\"font-weight: normal;\"><span style=\"background-color: rgb(255, 255, 255);\"><span style=\"\" color=\"\"><a href=\"http://seredun94.ecommtools.com/buy/2/?pselect=robokassa\" target=\"\">Оформить предзаказ</a>&nbsp;</span></span></span></h2>\r\n\r\n<p style=\"text-align: center;\"><span style=\"font-weight: normal;\"><span style=\"background-color: rgb(255, 255, 255);\"><span style=\"\" color=\"\">(от 3000 р.)</span></span></span></p>\r\n','sozdanie-i-oformlenie-soobshhestv-vkontakte','Как правильно создать и оформить сообщества в ВК','Создание групп ВК, оформление групп ВК.',0,'ru',NULL,''),(54,'','',32,16,17,2,32,'sozdanie-i-oformlenie-soobshestv-v-facebook',NULL,1,'Создание и оформление сообществ в Facebook','<p>В разработке<br></p>','Создание и оформление сообществ в Facebook','','',0,'ru',NULL,''),(55,'','',32,18,19,2,32,'sozdanie-i-oformlenie-soobshestv-v-odnoklassnikah',NULL,1,'Создание и оформление сообществ в Одноклассниках','<p>В разработке</p>','Создание и оформление сообществ в Одноклассниках','','',0,'ru',NULL,''),(56,'','',32,20,21,2,32,'sozdanie-i-oformlenie-akkaunta-v-instagram',NULL,1,'Создание и оформление аккаунта в Instagram','<p>В разработке</p>','Создание и оформление аккаунта в Instagram','','',0,'ru',NULL,''),(57,'','',32,22,23,2,32,'sozdanie-i-oformlenie-kanala-v-youtube',NULL,1,'Создание и оформление канала в Youtube','<p>В разработке</p>','Создание и оформление канала в Youtube','','',0,'ru',NULL,''),(58,'','',32,24,25,2,32,'sozdanie-i-oformlenie-akkaunta-v-twitter',NULL,1,'Создание и оформление аккаунта в Twitter','<p>В разработке</p>','Создание и оформление аккаунта в Twitter','','',0,'ru',NULL,''),(59,'','',32,26,27,2,32,'sozdanie-i-oformlenie-soobshestv-v-google-plus',NULL,1,'Создание и оформление сообществ в Google+','<p>В разработке</p>','Создание и оформление сообществ в Google+','','',0,'ru',NULL,''),(60,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?111\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"500\"}, 54814251);\r\n</script>\r\n\r\n<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?111\"></script>','',60,1,2,1,0,'AVK',NULL,1,'АВК','','АВК Украина','','',0,'ru',NULL,''),(61,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"500\"}, 59797579);\r\n</script>','',61,1,2,1,0,'IQ-Option',NULL,1,'IQ-Option','','IQ-Option','','',0,'ru',NULL,''),(62,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"500\"}, 63964799);\r\n</script>','',62,1,2,1,0,'zhizn-menjaetsja',NULL,1,'Жизнь меняется ','','Жизнь меняется ','','',0,'ru',NULL,''),(63,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"500\"}, 39875953);\r\n</script>','',63,1,2,1,0,'domotoy-ru',NULL,1,'Domotoy.ru','','Domotoy.ru','','',0,'ru',NULL,''),(64,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"500\"}, 48200734);\r\n</script>','',64,1,2,1,0,'cleverpub',NULL,1,'Cleverpub','','Cleverpub','','',0,'ru',NULL,''),(65,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"400\"}, 15412444);\r\n</script>','',65,1,2,1,0,'mrjones-ru',NULL,1,'MrJones.ru','','MrJones.ru','','',0,'ru',NULL,''),(66,'<div id=\"ok_group_widget\"></div>\r\n<script>\r\n!function (d, id, did, st) {\r\n  var js = d.createElement(\"script\");\r\n  js.src = \"http://connect.ok.ru/connect.js\";\r\n  js.onload = js.onreadystatechange = function () {\r\n  if (!this.readyState || this.readyState == \"loaded\" || this.readyState == \"complete\") {\r\n    if (!this.executed) {\r\n      this.executed = true;\r\n      setTimeout(function () {\r\n        OK.CONNECT.insertGroupWidget(id,did,st);\r\n      }, 0);\r\n    }\r\n  }}\r\n  d.documentElement.appendChild(js);\r\n}(document,\"ok_group_widget\",\"56812912836666\",\"{width:650,height:500}\");\r\n</script>\r\n','',66,1,2,1,0,'nivea',NULL,1,'Nivea','','Nivea','','',0,'ru',NULL,''),(67,'<script type=\"text/javascript\" src=\"//vk.com/js/api/openapi.js?112\"></script>\r\n\r\n<!-- VK Widget -->\r\n<div id=\"vk_groups\"></div>\r\n<script type=\"text/javascript\">\r\nVK.Widgets.Group(\"vk_groups\", {mode: 2, width: \"650\", height: \"400\"}, 29013846);\r\n</script>','',67,1,2,1,0,'dns',NULL,1,'DNS','','DNS','','',0,'ru',NULL,'');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `url` varchar(200) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
INSERT INTO `portfolio` VALUES (5,'2014-02-17 11:10:56','MrJones.ru','f11beae2cae7bbca8688da28908a0367.jpg',1,'mrjones-ru','ru'),(13,'2014-02-18 09:48:46','робота 1','f12ac3181eb88cf6c5afd36ad3b72483.jpg',1,'','uk'),(14,'2014-02-18 09:48:53','робота 2','ebdc9dc6ba3794d30e2f4e9160525c58.jpg',1,'','uk'),(15,'2014-02-18 09:48:59','робота 3','0ba145e3932aa027c336bb612bf7aaaf.jpg',1,'','uk'),(16,'2014-02-18 09:50:07','робота 4','8dd11989b5f3eed69f108c2048cfa288.jpg',1,'','uk'),(17,'2014-02-18 09:50:23','робота 5','5e5bc5d943a7d1137f946f8978ce97a5.jpg',1,'','uk'),(18,'2014-03-18 10:18:35','АВК','8bed6bb3526ab7fe2e0246d3dea19577.jpg',1,'AVK','ru'),(19,'2014-03-18 18:25:12','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','9dad7451a1f962e44869669d8fdcd946.png',1,'wadwad','en'),(20,'2014-03-18 19:51:34','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','ce5f6add98df6c2b5a7ec7977b3cf773.png',1,'adsadsa','en'),(21,'2014-03-18 19:51:50','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','7f2ffe14b1ffddcf64289732f2e55c3a.png',1,'dssadsadas','en'),(22,'2014-03-18 19:52:10','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','731c097fea145f313a575e5ac8b8dbdd.png',1,'adsadas','en'),(23,'2014-03-18 19:52:22','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','2a6993b43ac3bc984f77640eaa4979a2.png',1,'asdasdas','en'),(24,'2014-03-18 19:52:35','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','47fd755ec37efc62f549e5a2989c2ff9.png',1,'ggdsaf','en'),(25,'2014-03-18 19:52:55','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','4b6039b716b5356510539d6d4ed733a4.png',1,'sdadasdas','en'),(26,'2014-03-18 19:53:05','The PAGES - Past Global Changes project is an international effort to coordinate and promote past gl','6e2852d6fc7770389bcb607e5e347151.png',1,'sdsadas','en'),(29,'2014-03-24 20:15:25','IQ Option','bba0a7540ffe4a1b7c8c66e779bcd503.png',1,'IQ-Option','ru'),(30,'2014-03-25 22:13:29','Nivea','1f35b936a75354382a31d87c4e426c07.jpg',1,'nivea','ru'),(31,'2014-03-25 22:13:50','Cleverpub','600bf03849f455f8437f9d3de4dadeb5.jpg',1,'cleverpub','ru'),(32,'2014-03-25 22:14:00','Жизнь меняется','3eab54796b572bf0085701dfecd23620.jpg',1,'zhizn-menjaetsja','ru'),(33,'2014-04-26 20:13:56','Domotoy.ru','65054b356b65d18698131986b13babfd.jpg',1,'domotoy-ru','ru'),(34,'2014-04-26 21:07:50','DNS','63b25a4a1de24f74c27731eac59518ee.jpg',1,'dns','ru');
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `author` varchar(100) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,'2014-02-14 15:23:36','Движухой было раскручено  три наших фирменных вечеринки, приведено сотни клиентов. Огромное спасибо Евгению. Радует то что он одинаково универсально работают со всеми видами бизнеса.','b096965001226fde27d4cb8308cd49fb.png',1,'Марина Семакина','ru'),(4,'2014-02-14 15:23:36','Процесс разработка и внедрения креативной стратегии удался! Узнаваемость бренда растет и раскрутка проходит в соответствии с нашими целями и задачами. Спасибо за выход на новый уровень!','53640ede2f57d40073cd8fe570dc2957.png',1,'Надежда Павловна','ru'),(5,'2014-02-14 15:23:36','Ребята раскручивали мой интернет-магазин. Я получил более двухсот заказов из социальных сетей при минимальных вложениях. Спасибо им за труд, надеюсь будем еще с ними сотрудничать) \r\n\r\n\r\n','fdc843dc295544a9daf1f8e7076a5150.png',1,'Дмитрий Мартиросян','ru'),(7,'2014-02-18 09:52:56','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','005f0a4b34f0f3c53a4f0156227df926.jpg',1,'Жанна Д\'Арк','uk'),(8,'2014-02-18 09:53:02','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','f91e9a05a7cb90951ad131e6f5025526.jpg',1,'Долорес ибаррури','uk'),(9,'2014-02-18 09:53:05','What\'s the matter with the clothes I\'m wearing?\r\nCan\'t you tell that your tie\'s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin\' out lately, honey?\r\nYou can\'t dress trashy till you spend a lot of money.\r\nEverybody\'s talkin\' \'bout the new sound\r\nFunny, but it\'s still rock and roll to me','77f7ff49f571ad590147491310dfb58d.jpg',1,'Жанна Д\'Арк','uk'),(10,'2014-03-18 19:45:03','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','ff0bae2aae36809240543402d9215eb9.png',1,'Seredun','en'),(11,'2014-03-18 19:46:35','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','bf431802eeeab5537696c844a11fbd29.png',1,'Marina','en'),(12,'2014-03-18 19:46:51','The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.','d66e90a5d489a5287b86a7dc46b4dfc3.png',1,'Pasha','en');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1393229738),('m140221_083540_add_template_for_meta',1393229761),('m140221_084301_rename_custom_fields',1393229761),('m140221_104456_add_templates_for_another_meta_fields',1393229761),('m140224_075512_add_redirect_field_to_pages',1393235807),('m140224_110102_add_lang_field_to_meta',1393238053),('m140224_124401_add_admin_email_to_meta',1393317176),('m140303_081003_add_module_field_to_page',1393841201);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `joined_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` tinyint(1) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('','',1,'futurity','','7371014fa9783e7a24d5cd4b92fd52a0','2014-02-03 10:32:12',1,NULL),('','',2,'god','','81dc9bdb52d04dc20036dbd8313ed055','2014-02-03 10:32:12',1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-16 18:12:02
