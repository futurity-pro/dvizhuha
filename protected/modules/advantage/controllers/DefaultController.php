<?php

class DefaultController extends CoreController
{
    public  $imageWidth = 289;
    public  $imageHeight = 220;

    public $baseModel = 'Advantage';
    public $crudFormID = 'advantage-form';
    public $multiLang = true;
}