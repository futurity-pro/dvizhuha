<?php

class CategoryAdminController extends CoreController
{
    public  $baseModel = 'BlogCategory';
    public $crudFormID = 'blog-category-form';
    public $multiLang  =  true;

}