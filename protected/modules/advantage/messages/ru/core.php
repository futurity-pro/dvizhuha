<?php
/**
 * @file core.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 12.02.14 21:09
 */
return array(
    'Manage Advantage' => 'Управление преимуществами',
    'Advantage' => 'Преимущества',
    'ID' => 'ID',
    'Title' => 'Заголовок',
    'Created' => 'Дата создания',
    'Text' => 'Текст',
    'Visible' => 'Видимость',
    'Create' => 'Создать',
    'Edit' => 'Редактировать',
    'Edit Item' => 'Редактировать преимущество',
    'Create Item' => 'Создать преимущество',
    'Image' => 'Изображение',
    'Choose' => 'Выбрать',
    'No file' => 'Файл не выбран',
    'Drop files here or click to choose' => 'Перетащите сюда файл или кликните чтобы выбрать',
    'Save' => 'Сохранить',
    'Cancel' => 'Отмена',
    'Click here to choose file' => 'Нажмите здесь для выбора файла',

);