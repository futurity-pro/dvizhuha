<?php
/**
 * @file modules_default.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 12.02.14 21:07
 */
return array(
    'gii'=>array(
        'class'=>'system.gii.GiiModule',
        'password' => 'futurity77',
        //'ipFilters'=>array('127.0.0.1','::1'),
        'generatorPaths' => array(
            'bootstrap.gii',
        ),
    ),

    'yiicCommandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationPath' => 'application.migrations',
            'migrationTable' => 'migrations',
            'connectionID' => 'db',
            'templateFile' => 'application.migrations.template',
        ),
    ),
);