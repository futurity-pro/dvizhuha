<?php

class BlogCategory extends CActiveRecord
{

    public function tableName()
    {
        return 'blog_category';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 100),
            array('id, name', 'safe', 'on' => 'search'),
            array('lang', 'safe'),
        );
    }

    public function relations()
    {

        return array(
            'blogPosts' => array(self::HAS_MANY, 'BlogPosts', 'category_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => t('Name'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider(
            $this, array('criteria' => $criteria)
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public static function getCategories()
    {
        return CHtml::listData(BlogCategory::model()->findAll(), 'id', 'name');
    }

    public function defaultScope() {
        return array(
            'condition' => "bc.lang='".Yii::app()->language."'",
            'alias' => 'bc'
        );
    }


    public function lang($lang){
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => "bc. lang='$lang'",
            )
        );
        return $this;
    }
}
