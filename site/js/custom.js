/**
 * @file
 * @author Andrey Bogdanov <jekinen@yandex.com>
 * @created 14.02.14 18:49
 */
$(function() {

    $('.carousel-inner .item:first-child').addClass('active');
    if($('#myCarousel').length > 0) {
        $('#myCarousel').carousel({
            interval: 10000
        });
    }
    $(window).scrollTop(0);
});