<% if(!$model->isNewRecord): %>
    <div class="span4">
        <% $aceUploadContainerClass = 'ace-file-input ace-file-multiple'; %>
        <% $aceUploadContainerClass .= (!empty($model->image)) ? '' : ' ' . 'hidden' %>

        <div  class="<% echo $aceUploadContainerClass %>" id="ace-upload-container">
            <label for="" class="hide-placeholder selected">
        <span>
            <img src="<% echo $model->getImagePath() %>" style="width: 50px; height: 50px" id="model-image" class="middle" src=""/>
            <i class="icon-picture"></i>
        </span>
            </label>
            <a data-id="<% echo $model->id %>" class="cancel-upload remove" href="<% echo $this->createDefaultModuleAction('cancelUpload')%>" >
                <i class="icon-remove"></i>
            </a>
        </div>

        <div class="clearfix"></div>
        <%

         $baseModel = new $this->baseModel;
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFile',
                'config' => array(
                    'action' => Yii::app()->createUrl($this->createDefaultModuleAction('UploadImage')),
                    'allowedExtensions' => array("jpg", "jpeg", "gif", "png"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
                            $('#model-image').attr('src', '". $baseModel::getUploadDir() ."'+responseJSON.filename);
                            $.post('".Yii::app()->createUrl( '/' . strtolower($this->baseModel) . '/default/changeImage')."?id=".$model->id."', {filename: responseJSON.filename});
                            $('#ace-upload-container').removeClass('hidden');
                        }"
                )
            ));
        %>
    </div>

    <div class="hidden" id="choose-file-title">
        <% echo t('Click here to choose file')%>
    </div>

    <script>
        $(function() {
            $('.cancel-upload').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    'url' : $(this).attr('href'),
                    'data' :  {'id' : $(this).data('id')},
                    success : function (){
                        $('#ace-upload-container').remove()
                    }
                })
            });
        });
    </script>
<% endif; %>
<div class="clearfix"></div>