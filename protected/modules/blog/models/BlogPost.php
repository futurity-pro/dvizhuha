<?php

class BlogPost extends CActiveRecord
{
    public $authorSearch;
    public $categorySearch;

    public function tableName()
    {
        return 'blog_posts';
    }

    public function rules()
    {
        return array(
            array('title, text', 'required'),

            array('visible', 'numerical', 'integerOnly' => true),

            array('title', 'length', 'max' => 100),

            array('created_at', 'date', 'format' => 'dd/mm/yyyy'),

            array('user_id, lang, category_id', 'safe'),

            array(
                'id, title, text, created_at, user_id,
                visible, lang, authorSearch, categorySearch',
                'safe', 'on' => 'search'
            ),
        );
    }

    function relations() {
        return array(
            'author' => array( self::BELONGS_TO, 'User', 'user_id' ),
            'category' => array( self::BELONGS_TO, 'BlogCategory', 'category_id'),
        );
    }

    public function beforeSave()
    {
        if(!$this->isNewRecord) {
            $this->created_at = date(
                'Y-m-d H:i:s',
                CDateTimeParser::parse($this->created_at, 'dd/MM/yyyy')
            );
        } else {
            $this->created_at = new CDbExpression('NOW()');
        }
        return parent::beforeSave();
    }

    public function beforeValidate()
    {
        $this->user_id = Yii::app()->user->id;
        $this->user_id = 1;
        return parent::BeforeValidate();
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title'      => t('Title'),
            'text'       => t('Text'),
            'created_at' => t('Created At'),
            'categorySearch' => t('Category'),
            'category_id'  => t('Category'),
            'authorSearch' => t('Author'),
            'visible'    => t('Visible'),
            'lang'       => t('Lang'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->with = array('author', 'category');
        $criteria->compare('author.email', $this->authorSearch, true );
        $criteria->compare('category.name', $this->categorySearch, true);
        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('visible', $this->visible);

        return new CActiveDataProvider( $this, array(
            'criteria' => $criteria,
            'sort'=>array(
                'attributes'=>array(
                    'authorSearch' => array(
                        'asc' => 'author.email',
                        'desc' => 'author.email DESC',
                    ),
                    'categorySearch' => array(
                        'asc'=> 'category.name',
                        'desc' => 'category.name DESC',
                    ),

                    '*',
                ),
            ),
        ));
    }

    public function afterFind()
    {
        $this->created_at = $this->getCreatedAt('dd/MM/yyyy');
        return parent::afterFind();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getCreatedAt($format = '')
    {
        if (!empty($format)) {
            return Yii::app()->dateFormatter->format(
                $format, $this->created_at
            );
        } else {
            return Yii::app()->dateFormatter->format(
                'dd.MM.yyyy',
                $this->created_at
            );
        }
    }

    public function getVisible()
    {
        return ($this->visible) ? t('Yes') : t('No');
    }

    public function defaultScope() {
        return array(
            'condition' => "bp.lang='".Yii::app()->language."'",
            'alias' => 'bp'
        );
    }

    public function lang($lang){
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => "bp.lang='$lang'",
                'alias' => 'bp'
            )
        );
        return $this;
    }
}
