<% $this->beginContent(FuturityModule::pathToBlock('layouts::form')); %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::text-field'),
        array(
            'model' => $model,
            'name' => 'title',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::drop-down'),
        array(
            'model'   => $model,
            'name'    => 'category_id',
            'options' => CHtml::listData(BlogCategory::model()->findAll(), 'id', 'name')
        ));
    %>


    <% $this->renderPartial( FuturityModule::pathToBlock('fields::wysiwyg-field'),
        array(
            'model' => $model,
            'name' => 'text',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::date-field'),
        array(
            'model' => $model,
            'name' => 'created_at',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('fields::checkbox-field'),
        array(
            'model' => $model,
            'name' => 'visible',
        ));
    %>

    <% $this->renderPartial( FuturityModule::pathToBlock('form::form-actions'), array('model' => $model)); %>

<% $this->endContent(); %>
