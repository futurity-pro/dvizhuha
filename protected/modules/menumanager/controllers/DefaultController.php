<?php
class DefaultController extends CoreController
{
    public $baseModel = 'MenuItem';
    public $crudFormID = 'menu-item-form';

    public function actionAdmin($id = null)
    {
        if(!empty($id)) {
            $model = new MenuItem('search');

            $model->unsetAttributes();
            $model->menu_id = $id;
            $menu = Menu::model()->findByPk($id);

            if(isset($_GET[$this->baseModel]))
                $model->attributes = $_GET[$this->baseModel];

            $this->render('admin', array(
                'model'    => $model,
                'menuName' => $menu->name
                )
            );
        } else {
           $this->redirect('/menumanager/manage/');
        }
    }

    public function actionCreate($id = null)
    {
        if (!empty($id)) {
            $model = new MenuItem();
            $model->menu_id = $id;
            //$model->lang = Yii::app()->language;

            $this->performAjaxValidation($model);

            if (isset($_POST['MenuItem'])) {
                $model->attributes=$_POST['MenuItem'];
                if($model->save())
                    $this->redirect(array('admin', 'id'=> $id));
            }

            $this->render('create', array('model' => $model));
        } else {
            throw new CHttpException(500, 'Bad request');
        }
    }

}



