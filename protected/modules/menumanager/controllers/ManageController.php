<?php

class ManageController extends CoreController
{
    public $baseModel = 'Menu';
    public $crudFormID = 'menu-form';
    public $multiLang = true;
}
