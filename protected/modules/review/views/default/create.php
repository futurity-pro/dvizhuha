<div class="page-header position-relative">
    <h1>
        <% echo CHtml::link(tm('Review'), array('/review'));%>
        <small>
            <i class="icon-double-angle-right"></i>
            <% echo tm('Create Item')%>
        </small>
    </h1>
</div>
<% $this->renderPartial('_form', array('model'=>$model)); %>