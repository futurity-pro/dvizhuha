-- phpMyAdmin SQL Dump
-- version 4.0.8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 20 2014 г., 18:00
-- Версия сервера: 5.6.12
-- Версия PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `dvijuha`
--

-- --------------------------------------------------------

--
-- Структура таблицы `advantages`
--

CREATE TABLE IF NOT EXISTS `advantages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `text` text,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `url` varchar(200) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `advantages`
--

INSERT INTO `advantages` (`id`, `created_at`, `title`, `text`, `image`, `visible`, `url`, `lang`) VALUES
(1, '2014-02-14 15:23:36', 'Преимущество 1', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', '768baf14e7a0f6cff9c5a5b1fc7d5de5.jpg', 1, '/preimushestva/preimushestvo1', 'ru'),
(3, '2014-02-14 15:23:36', 'Преимущество 3', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', '9db17ed556dfb48f985190dbcb43f9b3.jpg', 1, '/preimushestva/preimushestvo1', 'ru'),
(6, '2014-02-14 15:23:36', 'Преимущество 2', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', '768baf14e7a0f6cff9c5a5b1fc7d5de5.jpg', 1, '/preimushestva/preimushestvo1', 'ru'),
(7, '2014-02-18 08:51:34', 'advantage 1', 'The PAGES - Past Global Changes project is an international effort to coordinate and promote past global change research. PAGES mission is to improve our understanding of past changes in the Earth System in a quantitative and process-oriented way in order to improve projections of future climate and environment, and inform strategies for sustainability.\r\nPAGES brings together more than 5,000 scientists from over 100 countries.\r\nPAGES scope of interest includes the physical climate system, biogeochemical cycles, ecosystem processes, biodiversity, and human dimensions, on different time scales — Pleistocene, Holocene, last millennium and the recent past.\r\nPAGES encourages international and interdisciplinary collaborations and seeks to involve scientists from developing countries in the worldwide paleo-community. In this respect, PAGES is not a research institution i.e. research activities are not prescribed by PAGES committees or the International Project Office (IPO), rather based on input from its multidisciplinary community, PAGES works to identify and understand those aspects of past climate and environmental change that are of greatest significance for the future of human societies.', 'b763012e16749777980b2631756811cf.jpg', 1, '', 'en'),
(8, '2014-02-18 09:51:14', 'дуже сильна сторона', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', 'b29eb7058f616ab1bb235bf81419c522.jpg', 1, '', 'uk'),
(9, '2014-02-18 09:51:20', 'дуже сильна сторона', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', '7065f7697b2951c1946a26fe2a82a018.jpg', 1, '', 'uk'),
(10, '2014-02-18 09:51:49', 'дуже сильна сторона', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', 'fbffac4b099785316299c9dbd9211c1e.jpg', 1, '', 'uk'),
(11, '2014-02-18 09:52:09', 'дуже сильна сторона', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', '6942106f27543a2e9d5b30889ca0e086.jpg', 1, '', 'uk');

-- --------------------------------------------------------

--
-- Структура таблицы `custom_fields`
--

CREATE TABLE IF NOT EXISTS `custom_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_key` varchar(100) NOT NULL,
  `value` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

--
-- Дамп данных таблицы `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `field_key`, `value`, `title`, `lang`) VALUES
(1, 'indexPageTitle', 'indexPageTitle', 'indexPageTitle', 'ru'),
(2, 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'ru'),
(3, 'contactWithUs', 'Связаться с нами', 'текст на кнопке Связаться с нами', 'ru'),
(4, 'ownStrongSides', 'ownStrongSides', 'ownStrongSides', 'ru'),
(5, 'ownWorksTitle', 'Наши работы', 'заголовок `Наши работы`', 'ru'),
(6, 'clientsReviewTitle', 'clientsReviewTitle', 'clientsReviewTitle', 'ru'),
(7, 'phoneOnHeader', 'phoneOnHeader', 'phoneOnHeader', 'ru'),
(8, 'emailOnHeader', 'emailOnHeader', 'emailOnHeader', 'ru'),
(9, 'bookServiceNowTitle', 'bookServiceNowTitle', 'bookServiceNowTitle', 'ru'),
(10, 'vkLink', 'vkLink', 'ccылка на vk', 'ru'),
(11, 'phoneOnFooter', 'phoneOnFooter', 'phoneOnFooter', 'ru'),
(12, 'emailOnFooter', 'emailOnFooter', 'emailOnFooter', 'ru'),
(13, 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'ru'),
(14, 'subscribeLinkOnFooter', 'http://futurity.us3.list-manage.com/subscribe?u=81d6f7a8cf84b15233dfd6f33&id=1fdaccc3ca', 'Линки подписки на рассылку в футере', 'ru'),
(15, 'facebookLink', 'facebookLink', 'facebookLink', 'ru'),
(16, 'youtubeLink', 'youtubeLink', 'youtubeLink', 'ru'),
(17, 'piLink', 'piLink', 'piLink', 'ru'),
(18, 'instagrammLink', 'instagrammLink', 'instagrammLink', 'ru'),
(19, 'InSocialLink', 'InSocialLink', 'InSocialLink', 'ru'),
(20, 'ssSocialLink', 'ssSocialLink', 'ssSocialLink', 'ru'),
(21, 'vimeoLink', 'vimeoLink', 'vimeoLink', 'ru'),
(22, 'twitterSocialLink', 'twitterSocialLink', 'twitterSocialLink', 'ru'),
(23, 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'ru'),
(24, 'copyRightOnFooter', 'copyRightOnFooter', 'copyRightOnFooter', 'ru'),
(25, 'indexPageTitle', 'indexPageTitle', 'indexPageTitle', 'en'),
(26, 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'en'),
(27, 'contactWithUs', 'contactWithUs', 'contactWithUs', 'en'),
(28, 'ownStrongSides', 'ownStrongSides', 'ownStrongSides', 'en'),
(29, 'ownWorksTitle', 'ownWorksTitle', 'ownWorksTitle', 'en'),
(30, 'clientsReviewTitle', 'clientsReviewTitle', 'clientsReviewTitle', 'en'),
(31, 'phoneOnHeader', 'phoneOnHeader', 'phoneOnHeader', 'en'),
(32, 'emailOnHeader', 'emailOnHeader', 'emailOnHeader', 'en'),
(33, 'bookServiceNowTitle', 'bookServiceNowTitle', 'bookServiceNowTitle', 'en'),
(34, 'vkLink', 'vkLink', 'vkLink', 'en'),
(35, 'phoneOnFooter', 'phoneOnFooter', 'phoneOnFooter', 'en'),
(36, 'emailOnFooter', 'emailOnFooter', 'emailOnFooter', 'en'),
(37, 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'en'),
(38, 'subscribeLinkOnFooter', 'subscribeLinkOnFooter', 'subscribeLinkOnFooter', 'en'),
(39, 'facebookLink', 'facebookLink', 'facebookLink', 'en'),
(40, 'youtubeLink', 'youtubeLink', 'youtubeLink', 'en'),
(41, 'piLink', 'piLink', 'piLink', 'en'),
(42, 'instagrammLink', 'instagrammLink', 'instagrammLink', 'en'),
(43, 'InSocialLink', 'InSocialLink', 'InSocialLink', 'en'),
(44, 'ssSocialLink', 'ssSocialLink', 'ssSocialLink', 'en'),
(45, 'vimeoLink', 'vimeoLink', 'vimeoLink', 'en'),
(46, 'twitterSocialLink', 'twitterSocialLink', 'twitterSocialLink', 'en'),
(47, 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'en'),
(48, 'copyRightOnFooter', 'copyRightOnFooter', 'copyRightOnFooter', 'en'),
(49, 'indexPageTitle', 'indexPageTitle', 'indexPageTitle', 'uk'),
(50, 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'uk'),
(51, 'contactWithUs', 'contactWithUs', 'Связаться с нами', 'uk'),
(52, 'ownStrongSides', 'ownStrongSides', 'ownStrongSides', 'uk'),
(53, 'ownWorksTitle', 'ownWorksTitle', 'ownWorksTitle', 'uk'),
(54, 'clientsReviewTitle', 'clientsReviewTitle', 'clientsReviewTitle', 'uk'),
(55, 'phoneOnHeader', 'phoneOnHeader', 'phoneOnHeader', 'uk'),
(56, 'emailOnHeader', 'examle@email.com', 'emailOnHeader', 'uk'),
(57, 'bookServiceNowTitle', 'bookServiceNowTitle', 'bookServiceNowTitle', 'uk'),
(58, 'vkLink', 'vkLink', 'vkLink', 'uk'),
(59, 'phoneOnFooter', 'phoneOnFooter', 'phoneOnFooter', 'uk'),
(60, 'emailOnFooter', 'emailOnFooter', 'emailOnFooter', 'uk'),
(61, 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'uk'),
(62, 'subscribeLinkOnFooter', 'http://futurity.us3.list-manage.com/subscribe?u=81d6f7a8cf84b15233dfd6f33&id=1fdaccc3ca', 'subscribeLinkOnFooter', 'uk'),
(63, 'facebookLink', 'facebookLink', 'facebookLink', 'uk'),
(64, 'youtubeLink', 'youtubeLink', 'youtubeLink', 'uk'),
(65, 'piLink', 'piLink', 'piLink', 'uk'),
(66, 'instagrammLink', 'instagrammLink', 'instagrammLink', 'uk'),
(67, 'InSocialLink', 'InSocialLink', 'InSocialLink', 'uk'),
(68, 'ssSocialLink', 'ssSocialLink', 'ssSocialLink', 'uk'),
(69, 'vimeoLink', 'vimeoLink', 'vimeoLink', 'uk'),
(70, 'twitterSocialLink', 'twitterSocialLink', 'twitterSocialLink', 'uk'),
(71, 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'uk'),
(72, 'copyRightOnFooter', 'copyRightOnFooter', 'copyRightOnFooter', 'uk'),
(73, 'contactWithUsLink', 'contactWithUsLink', 'contactWithUsLink', 'ru'),
(74, 'contactWithUsTitle', 'contactWithUsTitle', 'contactWithUsTitle', 'ru'),
(75, 'contactWithUsText', 'contactWithUsText', 'contactWithUsText', 'ru'),
(76, 'fieldRequiredForFillingText', 'fieldRequiredForFillingText', 'fieldRequiredForFillingText', 'ru'),
(77, 'contactWithUsTitle', 'contactWithUsTitle', 'contactWithUsTitle', 'uk'),
(78, 'contactWithUsText', 'contactWithUsText', 'contactWithUsText', 'uk'),
(79, 'fieldRequiredForFillingText', 'fieldRequiredForFillingText', 'fieldRequiredForFillingText', 'uk'),
(80, 'phoneOnHeader', 'phoneOnHeader', 'phoneOnHeader', ''),
(81, 'emailOnHeader', 'emailOnHeader', 'emailOnHeader', ''),
(82, 'bookServiceNowTitle', 'bookServiceNowTitle', 'bookServiceNowTitle', ''),
(83, 'contactWithUs', 'contactWithUs', 'contactWithUs', ''),
(84, 'phoneOnFooter', 'phoneOnFooter', 'phoneOnFooter', ''),
(85, 'emailOnFooter', 'emailOnFooter', 'emailOnFooter', ''),
(86, 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', 'subscribeOnEmailsTitle', ''),
(87, 'subscribeLinkOnFooter', 'subscribeLinkOnFooter', 'subscribeLinkOnFooter', ''),
(88, 'vkLink', 'vkLink', 'vkLink', ''),
(89, 'facebookLink', 'facebookLink', 'facebookLink', ''),
(90, 'youtubeLink', 'youtubeLink', 'youtubeLink', ''),
(91, 'piLink', 'piLink', 'piLink', ''),
(92, 'instagrammLink', 'instagrammLink', 'instagrammLink', ''),
(93, 'InSocialLink', 'InSocialLink', 'InSocialLink', ''),
(94, 'ssSocialLink', 'ssSocialLink', 'ssSocialLink', ''),
(95, 'vimeoLink', 'vimeoLink', 'vimeoLink', ''),
(96, 'twitterSocialLink', 'twitterSocialLink', 'twitterSocialLink', ''),
(97, 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', 'odnoklassnikiSocialLink', ''),
(98, 'copyRightOnFooter', 'copyRightOnFooter', 'copyRightOnFooter', ''),
(99, 'indexPageTitle', 'indexPageTitle', 'indexPageTitle', ''),
(100, 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', 'indexPageTextBelowTitle', ''),
(101, 'ownStrongSides', 'ownStrongSides', 'ownStrongSides', ''),
(102, 'ownWorksTitle', 'ownWorksTitle', 'ownWorksTitle', ''),
(103, 'clientsReviewTitle', 'clientsReviewTitle', 'clientsReviewTitle', '');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `menu_key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `lang`, `menu_key`) VALUES
(1, 'меню в хеадере', 'ru', 'header'),
(2, 'меню в футере', 'ru', 'footer'),
(3, 'header', 'en', 'header'),
(4, 'header', 'en', 'header'),
(5, 'footer', 'en', 'footer'),
(6, 'header', 'en', 'header'),
(7, 'footer', 'en', 'footer'),
(8, 'header', 'uk', 'header'),
(9, 'footer', 'uk', 'footer');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

CREATE TABLE IF NOT EXISTS `menu_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(1000) NOT NULL,
  `title` varchar(100) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `visible` int(1) unsigned NOT NULL DEFAULT '1',
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`id`, `url`, `title`, `order`, `visible`, `lang`, `menu_id`) VALUES
(2, 'kejsy', 'Кейсы', 2, 1, 'ru', 1),
(4, 'o-kompanii', 'Прайсы', 1, 1, 'ru', 1),
(5, 'o-kompanii', 'Цены', 4, 1, 'ru', 1),
(6, 'contacts', 'Связь', 3, 0, 'ru', 1),
(13, 'машины', 'машины', 4, 1, 'ru', 2),
(14, 'автобусы', 'автобусы', 6, 1, 'ru', 2),
(15, 'минивены', 'минивены', 3, 1, 'ru', 2),
(16, 'тягачи', 'тягачи', 5, 1, 'ru', 2),
(17, 'very-expensive-cars', 'Дорогие тачки', 2, 1, 'ru', 2),
(18, 'very-expensive-cars', 'Дорогие тачки', 1, 1, 'ru', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `id` varchar(50) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `customCodeHead` text,
  `customCodeAfterOpenBody` text,
  `customCodeBeforeCloseBody` text,
  `customCodeFooter` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meta`
--

INSERT INTO `meta` (`id`, `title`, `description`, `keywords`, `customCodeHead`, `customCodeAfterOpenBody`, `customCodeBeforeCloseBody`, `customCodeFooter`) VALUES
('Движуха', '123', '1234', '1234', '<script>\r\nconsole.log(''cutom code head'');\r\n</script>\r\n', '<script>\r\nconsole.log(''after open body'');\r\n</script>\r\n', '<script>\r\nconsole.log(''before close body'');\r\n</script>\r\n', '<script>\r\nconsole.log(''footer'');\r\n</script>');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customBeforeContent` text,
  `customAfterContent` text,
  `root` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(10) unsigned NOT NULL,
  `rgt` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `slug` varchar(127) NOT NULL,
  `layout` varchar(15) DEFAULT NULL,
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `page_title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 -page, 1 - folder',
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`),
  KEY `root` (`root`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `level` (`level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `customBeforeContent`, `customAfterContent`, `root`, `lft`, `rgt`, `level`, `parent_id`, `slug`, `layout`, `is_published`, `page_title`, `content`, `meta_title`, `meta_description`, `meta_keywords`, `type`, `lang`) VALUES
(13, '<script>console.log(''before content''); </script>', '<script>console.log(''after content''); </script>', 13, 1, 6, 1, 0, 'o-nas', NULL, 1, 'О нас', '<h1>О нас<br></h1>\r\n\r\n<p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p>\r\n<h2>Заголовок 2</h2>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p>\r\n<h3>Заголовок 3</h3>\r\n\r\n<p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p>\r\n<img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"><hr><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"><hr>\r\n<p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p>\r\n<h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4>\r\n\r\n<p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p>\r\n\r\n<ul>\r\n	<li>Aenean imperdiet lobortis luctus.</li>\r\n	<li>Sed nec lorem quam.</li>\r\n	<li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li>\r\n	<li>Ut adipiscing interdum massa, eget posuere eros pharetra. \r\n<ol>\r\n	<li>Aenean imperdiet lobortis luctus.</li>\r\n	<li>Aenean imperdiet lobortis luctus.</li>\r\n	<li>Ut adipiscing interdum massa, eget posuere eros pharetra \r\n<ul>\r\n	<li>Aenean imperdiet lobortis luctus.</li>\r\n	<li>Sed nec lorem quam.</li>\r\n</ul></li>\r\n</ol></li>\r\n</ul>\r\n<p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p>\r\n<hr>\r\n<p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p>\r\n<hr>\r\n<blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo. \r\n<p>Василий Пупкин</p>\r\n</blockquote>\r\n\r\n<table>\r\n\r\n<thead>\r\n	<tr>\r\n<th>Чарльз Дарвин</th>\r\n\r\n<th>Чарльз Дарвин</th>\r\n\r\n<th>Чарльз Дарвин</th>\r\n	</tr>\r\n</thead>\r\n\r\n<tbody>\r\n	<tr>\r\n		<td>Яблоки</td>\r\n\r\n		<td>Бананы</td>\r\n\r\n		<td>Маракуйя</td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td>Вишня</td>\r\n\r\n		<td>Клубника</td>\r\n\r\n		<td>Морской окунь</td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td>Театр</td>\r\n\r\n		<td>Космический</td>\r\n\r\n		<td>Тантронафт</td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td>Хрофт</td>\r\n\r\n		<td>Лесоповал</td>\r\n\r\n		<td>Сикомбрический</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n<br>\r\n\r\n<p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>\r\n<p></p>\r\n', 'О нас', '', '', 0, 'ru'),
(14, NULL, NULL, 14, 1, 6, 1, 0, 'kejsy', NULL, 1, 'Кейсы', '<h1>Заголовок 1<p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p></h1><h2>Заголовок 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p><h3>Заголовок 3</h3><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p><p><img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"></p><hr><p><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"></p><hr><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p><h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.<ol><li>Aenean imperdiet lobortis luctus.</li><li>Aenean imperdiet lobortis luctus.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra<ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li></ul></li></ol></li></ul><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p><hr><p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><hr><blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.<p>Василий Пупкин</p></blockquote><table><thead><tr><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th></tr></thead><tbody><tr><td>Яблоки</td><td>Бананы</td><td>Маракуйя</td></tr><tr><td>Вишня</td><td>Клубника</td><td>Морской окунь</td></tr><tr><td>Театр</td><td>Космический</td><td>Тантронафт</td></tr><tr><td>Хрофт</td><td>Лесоповал</td><td>Сикомбрический</td></tr></tbody></table><p><br></p><p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>', 'Кейсы', '', '', 0, 'ru'),
(15, NULL, NULL, 14, 2, 3, 2, 14, 'case1', NULL, 1, 'case1', '<h1>Заголовок 1<p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p></h1><h2>Заголовок 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p><h3>Заголовок 3</h3><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p><p><img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"></p><hr><p><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"></p><hr><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p><h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.<ol><li>Aenean imperdiet lobortis luctus.</li><li>Aenean imperdiet lobortis luctus.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra<ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li></ul></li></ol></li></ul><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p><hr><p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><hr><blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.<p>Василий Пупкин</p></blockquote><table><thead><tr><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th></tr></thead><tbody><tr><td>Яблоки</td><td>Бананы</td><td>Маракуйя</td></tr><tr><td>Вишня</td><td>Клубника</td><td>Морской окунь</td></tr><tr><td>Театр</td><td>Космический</td><td>Тантронафт</td></tr><tr><td>Хрофт</td><td>Лесоповал</td><td>Сикомбрический</td></tr></tbody></table><p><br></p><p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>\r\n', 'case1', '', '', 0, 'ru'),
(16, NULL, NULL, 14, 4, 5, 2, 14, 'case2', NULL, 1, 'case2', '<h1>Заголовок 1<p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p></h1><h2>Заголовок 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p><h3>Заголовок 3</h3><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p><p><img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"></p><hr><p><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"></p><hr><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p><h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.<ol><li>Aenean imperdiet lobortis luctus.</li><li>Aenean imperdiet lobortis luctus.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra<ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li></ul></li></ol></li></ul><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p><hr><p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><hr><blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.<p>Василий Пупкин</p></blockquote><table><thead><tr><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th></tr></thead><tbody><tr><td>Яблоки</td><td>Бананы</td><td>Маракуйя</td></tr><tr><td>Вишня</td><td>Клубника</td><td>Морской окунь</td></tr><tr><td>Театр</td><td>Космический</td><td>Тантронафт</td></tr><tr><td>Хрофт</td><td>Лесоповал</td><td>Сикомбрический</td></tr></tbody></table><p><br></p><p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>\r\n', 'case2', '', '', 0, 'ru'),
(19, NULL, NULL, 13, 2, 5, 2, 13, 'uslugi', NULL, 1, 'Услуги', '<h1>О компании<p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p></h1><h2>Заголовок 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p><h3>Заголовок 3</h3><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p><p><img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"></p><hr><p><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"></p><hr><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p><h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.<ol><li>Aenean imperdiet lobortis luctus.</li><li>Aenean imperdiet lobortis luctus.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra<ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li></ul></li></ol></li></ul><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p><hr><p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><hr><blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.<p>Василий Пупкин</p></blockquote><table><thead><tr><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th></tr></thead><tbody><tr><td>Яблоки</td><td>Бананы</td><td>Маракуйя</td></tr><tr><td>Вишня</td><td>Клубника</td><td>Морской окунь</td></tr><tr><td>Театр</td><td>Космический</td><td>Тантронафт</td></tr><tr><td>Хрофт</td><td>Лесоповал</td><td>Сикомбрический</td></tr></tbody></table><p><br></p><p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>\r\n', 'o-kompanii', '', '', 0, 'ru'),
(20, NULL, NULL, 13, 3, 4, 3, 19, 'usluga-1', NULL, 0, 'услуга 1', '', 'компания 1', '', '', 0, 'ru'),
(21, NULL, NULL, 21, 1, 4, 1, 0, 'preimushestva', NULL, 1, 'Преимущества', '<p>preimushestva<br></p>', 'preimushestva', '', '', 1, 'ru'),
(22, NULL, NULL, 21, 2, 3, 2, 21, 'preimushestvo1', NULL, 1, 'Преимущество1', '<h1>Наши сильные стороны<br><p><em>Lorem ipsum dolor sit amet</em>, consectetur adipiscing elit.&nbsp;<strong>Nulla ultrices luctus</strong>&nbsp;sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis</p></h1><h2>Заголовок 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada.</p><h3>Заголовок 3</h3><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue,</p><p><img src="http://eswalls.com/wp-content/uploads/2014/02/cute-baby-girl-usa-hd-wallpapers.jpeg" style="cursor: default;"></p><hr><p><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" class="img-responsive" alt="" style="box-sizing: border-box; display: block; color: rgb(65, 68, 80); font-family: ''Open Sans'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;"></p><hr><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui.</p><h4><img src="file:///C:/web/projects/dvijuha.futurity.loc/www/html/img/img-5.jpg" style="font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif; font-size: 15px; line-height: 1.45em; color: rgb(57, 57, 57);">Заголовок 4</h4><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra.<ol><li>Aenean imperdiet lobortis luctus.</li><li>Aenean imperdiet lobortis luctus.</li><li>Ut adipiscing interdum massa, eget posuere eros pharetra<ul><li>Aenean imperdiet lobortis luctus.</li><li>Sed nec lorem quam.</li></ul></li></ol></li></ul><p>Duis venenatis quam augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices luctus sagittis. Aenean id nibh urna.</p><hr><p>Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.</p><hr><blockquote>Так выделяется ссылка. Mauris egestas, est at tempus mollis, eros nulla faucibus tellus, ac rutrum lorem libero vitae dui. In vestibulum suscipit elit nec malesuada. Duis venenatis quam augue, sed vestibulum dui auctor id. Ut at sapien eu mi gravida commodo.<p>Василий Пупкин</p></blockquote><table><thead><tr><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th><th>Чарльз Дарвин</th></tr></thead><tbody><tr><td>Яблоки</td><td>Бананы</td><td>Маракуйя</td></tr><tr><td>Вишня</td><td>Клубника</td><td>Морской окунь</td></tr><tr><td>Театр</td><td>Космический</td><td>Тантронафт</td></tr><tr><td>Хрофт</td><td>Лесоповал</td><td>Сикомбрический</td></tr></tbody></table><p><a href="http://xn--%20-8cd3cfr1aae9hqa/" target="">это ссылка</a>&nbsp;</p>\r\n', 'Преимущество1', '', '', 0, 'ru'),
(25, NULL, NULL, 25, 1, 2, 1, 0, 'cini', NULL, 1, 'ціни', '<p>ціни<br></p>', 'ціни', '', '', 0, 'uk');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `url` varchar(200) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`id`, `created_at`, `title`, `image`, `visible`, `url`, `lang`) VALUES
(5, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(6, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(7, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(8, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(9, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(10, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(11, '2014-02-17 11:10:56', 'Описание проекта номер один', '01458cf0e13fe6d9695eda4f4f2a8329.jpg', 0, 'o-nas', 'ru'),
(12, '2014-02-18 09:48:09', 'робота 1', NULL, 1, '', 'ru'),
(13, '2014-02-18 09:48:46', 'робота 1', 'f12ac3181eb88cf6c5afd36ad3b72483.jpg', 1, '', 'uk'),
(14, '2014-02-18 09:48:53', 'робота 2', 'ebdc9dc6ba3794d30e2f4e9160525c58.jpg', 1, '', 'uk'),
(15, '2014-02-18 09:48:59', 'робота 3', '0ba145e3932aa027c336bb612bf7aaaf.jpg', 1, '', 'uk'),
(16, '2014-02-18 09:50:07', 'робота 4', '8dd11989b5f3eed69f108c2048cfa288.jpg', 1, '', 'uk'),
(17, '2014-02-18 09:50:23', 'робота 5', '5e5bc5d943a7d1137f946f8978ce97a5.jpg', 1, '', 'uk');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text,
  `image` varchar(100) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `author` varchar(100) DEFAULT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'ru',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `created_at`, `text`, `image`, `visible`, `author`, `lang`) VALUES
(1, '2014-02-14 15:23:36', 'Равным образом укрепление и развитие структуры способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Разнообразный и богатый опыт постоянное', 'e3026ee373900dc971b6e31798840c91.png', 1, 'Долорес Ибарули', 'ru'),
(4, '2014-02-14 15:23:36', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', 'be4c7d92756837f0555703a3e715e75a.png', 1, 'Жанна Д''Арк', 'ru'),
(5, '2014-02-14 15:23:36', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', 'ca7b4aa52552f56c42b85d8735d82648.png', 1, 'Жанна Д''Арк', 'ru'),
(6, '2014-02-14 15:23:36', 'Не следует, однако забывать, что дальнейшее развитие различных форм деятельности в\r\n                              значительной степени обуславливает создание новых предложений. Повседневная практика\r\n                              показывает, что новая модель организационной деятельности обеспечивает широкому кругу\r\n                              (специалистов) участие в формировании ущественных финансовых и административных условий.', '2cd8680672555bea4d0b53a2e0610c7f.png', 0, 'Жанна Д''Арк', 'ru'),
(7, '2014-02-18 09:52:56', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', '005f0a4b34f0f3c53a4f0156227df926.jpg', 1, 'Жанна Д''Арк', 'uk'),
(8, '2014-02-18 09:53:02', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', 'f91e9a05a7cb90951ad131e6f5025526.jpg', 1, 'Долорес ибаррури', 'uk'),
(9, '2014-02-18 09:53:05', 'What''s the matter with the clothes I''m wearing?\r\nCan''t you tell that your tie''s too wide?\r\nMaybe I should buy some old tab collars?\r\nWelcome back to the age of jive.\r\nWhere have you been hidin'' out lately, honey?\r\nYou can''t dress trashy till you spend a lot of money.\r\nEverybody''s talkin'' ''bout the new sound\r\nFunny, but it''s still rock and roll to me', '77f7ff49f571ad590147491310dfb58d.jpg', 1, 'Жанна Д''Арк', 'uk');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `menu_item_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
