<?php
/**
 * @file core.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 17.02.14 19:33
 */
return array(
    'Key' => 'Ключ',
    'Title' => 'Заголовок',
    'Manage Custom Fields' => 'Управление дополнительными полями',
    'Value' => 'Значение',
    'Create Item' => 'Создать объект',
    'Custom Fields' => 'Дополнительные поля',
    'Create' => 'Создать'
);