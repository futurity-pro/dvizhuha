
<div class="navbar navbar-fixed-top" id="admin-panel">
    <div class="navbar-inner">
        <div class="container-fluid">

                <ul class="brand admin-panel-navigation">
                    <li class="<% if(!$isBackend) echo 'active' %>"><a href="/"> <%  echo Yii::app()->name %></a></li>
                    <li class="<% if($isBackend) echo 'active' %>"> <a href="/admin"> <% echo t('Administrator') %></a></li>
                </ul>


            <div class="lang-switch">
                <a  title="Русский" href="/ru/pages/admin">ru</a>
                <a  title="English" href="/en/pages/admin">en</a>
                <a  title="Українська" href="/uk/pages/admin">uk</a>
            </div>

            <ul class="nav ace-nav pull-right">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="/site/img/no-photo.png">

                        <span class="user-info">
						    <small><?php echo Yii::app()->user->name ?></small>
						</span>

                        <i class="icon-caret-down"></i>
                    </a>

                    <?php $this->widget('bootstrap.widgets.TbDropdown', array(
                        'htmlOptions' => array(
                            'class' => 'user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer',
                        ),
                        'items' => array(
                            array(
                                'label' => t('Logout'),
                                'icon' => 'off',
                                'url' => array('/site/logout/')
                            ),
                        )
                    )); ?>
                </li>
            </ul>
            <!--/.ace-nav-->
        </div>
        <!--/.container-fluid-->
    </div>
    <!--/.navbar-inner-->
</div>
