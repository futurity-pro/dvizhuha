<?php
class DefaultController extends CoreController
{
    public $baseModel   = 'CustomField';
    public $crudFormID = 'custom-fields-form';
    public $multiLang = true;
}