<?php

class m140221_084301_rename_custom_fields extends CDbMigration
{

    public function up()
    {
        Yii::app()->db->pdoInstance->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $transaction = $this->getDbConnection()->beginTransaction();
        try {
           $sql = "
            ALTER TABLE  `meta` CHANGE  `customCodeHead`  `cm_head` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ALTER TABLE  `meta` CHANGE  `customCodeAfterOpenBody`  `cm_after_open_body` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ALTER TABLE  `meta` CHANGE  `customCodeBeforeCloseBody`  `cm_before_close_body` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ALTER TABLE  `meta` CHANGE  `customCodeFooter`  `cm_footer` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ALTER TABLE  `pages` CHANGE  `customBeforeContent`  `cm_before_content` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ALTER TABLE  `pages` CHANGE  `customAfterContent`  `cm_after_content` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
            ";

            $this->execute($sql);
            $transaction->commit();
        }

        catch(Exception $e) {
            echo "Exception: ".$e->getMessage()."\n";
            $transaction->rollBack();
            return false;
        }
    }

    public function safeDown() {
        $this->dropColumn('meta', 'meta_template');
    }
}