<?php
$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array('Error');
?>



<div class="homepage homepage-bg">
    <div class="container">
        <div class="container-bg">
            <div class="internal">
                <h1>Error <?php echo $code; ?></h1>
                <?php echo CHtml::encode($message); ?>
            </div>
        </div>
    </div>
</div>