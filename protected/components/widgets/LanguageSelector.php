<?php
/**
 * @file LanguageSelector.php
 * @author Bogdanov Andrey <jehkinen@ya.ru>
 * @created 18.02.14 14:29
 */
class LanguageSelector extends CWidget
{
    public function run()
    {
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;
        $this->render('languageSelector', array('currentLang' => $currentLang, 'languages'=>$languages));

    }
}