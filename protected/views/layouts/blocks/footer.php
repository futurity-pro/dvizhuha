<footer>
    <div class="footer">
        <div class="container">
            <h2><% echo CustomField::getCustomFieldByKey('bookServiceNowTitle')%></h2>
            <div class="footer-btn">
                <a href="/contact" class="btns">
                    <% echo CustomField::getCustomFieldByKey('contactWithUs')%>
                </a>
            </div>
            <div class="footer-top clearfix">
                <div class="col-sm-2">
                    <div class="footer-menu clearfix">
                        <ul>
                            <% foreach(Menu::getMenuByKey('footer') as $menuItem ):%>
                                <li><a href="<% echo $menuItem->url %>"><% echo $menuItem->title %></a></li>
                            <% endforeach;%>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="footer-contacts">
                        <p>
                            <span class="footer-phone">
                                <% echo CustomField::getCustomFieldByKey('phoneOnFooter')%>
                            </span>
                        </p>

                        <p>
                            <span class="footer-mail">
                                <a href="mailto:<% echo CustomField::getCustomFieldByKey('emailOnFooter')%>">
                                    <% echo CustomField::getCustomFieldByKey('emailOnFooter')%>
                                </a>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="footer-subscribe">
                        <p><% echo CustomField::getCustomFieldByKey('subscribeOnEmailsTitle')%></p>
                        <form action="<% echo CustomField::getCustomFieldByKey('subscribeLinkOnFooter')%>" method="post">
                            <div class="clearfix">
                                <div class="footer-form-input-out"><input type="email" name="MERGE0" class="form-control" placeholder="Ваш e-mail"></div>
                                <div class="footer-form-input-out-2"><button type="Submit" class="btn btn-default">ok</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer-social clearfix">
                <ul class="clearfix">
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('vkLink')%>" class="footer-social-vk"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('facebookLink')%>" class="footer-social-fb"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('youtubeLink')%>" class="footer-social-yt"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('piLink')%>" class="footer-social-pi"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('instagrammLink')%>" class="footer-social-ig"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('InSocialLink')%>" class="footer-social-li"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('ssSocialLink')%>" class="footer-social-ss"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('vimeoLink')%>" class="footer-social-vm"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('twitterSocialLink')%>" class="footer-social-tw"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('odnoklassnikiSocialLink')%>" class="footer-social-ok"></a></li>
                    <li class="col-md-1"><a target="_blank" href="<% echo CustomField::getCustomFieldByKey('googlePlusSocialLink')%>" class="footer-social-gp"></a></li>

                </ul>
            </div>
            <div class="footer-copyright clearfix">
                <% echo CustomField::getCustomFieldByKey('copyRightOnFooter')%>
                <div class="footer-copyright-futurity"><a href="http://futurity.pro/ru/"><% echo CustomField::getCustomFieldByKey('madeInFuturity')%></a></div>
            </div>
        </div>
    </div>
    <% echo $this->customCodeFooter; %>
</footer>